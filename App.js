/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './src/screens/Login';
import RegisterScreen from './src/screens/Login/Register';
import RecuperarPasswordScreen from './src/screens/Login/RecuperarPassword';
import DashboardScreen from './src/screens/Dashboard';
import CuentaScreen from './src/screens/Cuenta';
import CalendarioScreen from './src/screens/Calendario';
import NoticiasScreen from './src/screens/Noticias';
import SettingsScreen from './src/screens/Settings';
import TabBar from './src/components/TabBar';
import {UserContext} from './src/utils/UserContext';
import AeronavesListadoScreen from './src/screens/AeronavesListado';
import {colors, elevationShadowStyle, perfectSize} from './src/utils/styling';
import AlumnosListadoScreen from './src/screens/Alumnos';
import NotificacionesScreen from './src/screens/Notificaciones';
import InstructoresListadoScreen from './src/screens/Instructores';
import InstructoresDetalleScreen from './src/screens/Instructores/InstructoresDetalle';
import AprobarReservasScreen from './src/screens/Reservas/AprobarReservas';
import CrearReservaScreen from './src/screens/Reservas/CrearReserva';
import EditarAlumnoScreen from './src/screens/Alumnos/EditarAlumno';
import MantenimientosListadoScreen from './src/screens/Mantenimientos';
import {Button} from 'react-native-elements';
import CrearMantenimientoScreen from './src/screens/Mantenimientos/CrearMantenimiento';
import InstructorHorarioScreen from './src/screens/Instructores/InstructorHorario';
import AeronavesFormScreen from './src/screens/AeronavesListado/AeronavesForm';
import UsuariosListadoScreen from './src/screens/Usuarios';
import UsuariosFormScreen from './src/screens/Usuarios/UsuariosForm';
import AgendarCierrePistaScreen from './src/screens/Pistas/AgendarCierrePista';
import PistasListadoScreen from './src/screens/Pistas';
import ReportesListadoScreen from './src/screens/Reportes';
import TiposReportesListadoScreen from './src/screens/Reportes/TiposReportes';
import CargarReporteScreen from './src/screens/Reportes/CargarReporte';
import CambiarPasswordScreen from './src/screens/Login/CambiarPassword';
import DetalleReservaScreen from './src/screens/Reservas/DetalleReserva';
import PerfilScreen from './src/screens/Cuenta/Perfil';
import AtencionScreen from './src/screens/Atencion';
import ReservasAlumnoScreen from './src/screens/Reservas/ReservasAlumno';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Home() {
  return (
    <Tab.Navigator tabBar={props => <TabBar {...props} />}>
      <Tab.Screen name="Dashboard" component={DashboardScreen} />
      <Tab.Screen name="Cuenta" component={CuentaScreen} />
      <Tab.Screen name="Calendario" component={CalendarioScreen} />
      <Tab.Screen name="Noticias" component={NoticiasScreen} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  );
}

const App: () => React$Node = () => {
  const [user, setUser] = useState({});
  const changeUser = newUser => {
    setUser(newUser);
  };
  return (
    <UserContext.Provider value={{user, setUser: changeUser}}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Login"
          screenOptions={{
            headerStyle: {
              backgroundColor: colors.primary,
            },
            headerTitleAlign: 'center',
            headerTintColor: colors.white,
            headerTitleStyle: {
              fontFamily: 'Questrial-Regular',
              fontSize: perfectSize(24),
              letterSpacing: perfectSize(1.5),
              textTransform: 'uppercase',
            },
          }}>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Register"
            component={RegisterScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="CambiarPassword"
            component={CambiarPasswordScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="RecuperarPassword"
            component={RecuperarPasswordScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="AeronavesListado"
            options={({navigation, route}) => ({
              headerStyle: {
                borderBottomWidth: 0,
                ...elevationShadowStyle(0),
                backgroundColor: colors.primary,
              },
              headerTitle: 'Aeronaves',
              headerRight: () => {
                return (
                  <Button
                    type={'clear'}
                    icon={{
                      name: 'ios-add',
                      type: 'ionicon',
                      size: 30,
                      color: colors.white,
                    }}
                    onPress={() =>
                      navigation.navigate('AeronavesForm', {
                        editar: false,
                        refresh: route?.params?.refresh,
                      })
                    }
                  />
                );
              },
            })}
            component={AeronavesListadoScreen}
          />
          <Stack.Screen
            name="UsuariosListado"
            options={({navigation, route}) => ({
              headerStyle: {
                borderBottomWidth: 0,
                ...elevationShadowStyle(0),
                backgroundColor: colors.primary,
              },
              headerTitle: 'Usuarios',
              headerRight: () => {
                return (
                  <Button
                    type={'clear'}
                    icon={{
                      name: 'ios-add',
                      type: 'ionicon',
                      size: 30,
                      color: colors.white,
                    }}
                    onPress={() =>
                      navigation.navigate('UsuariosForm', {
                        editar: false,
                        refresh: route?.params?.refresh,
                      })
                    }
                  />
                );
              },
            })}
            component={UsuariosListadoScreen}
          />
          <Stack.Screen
            name="UsuariosForm"
            options={{
              headerTitle: 'Usuario',
            }}
            component={UsuariosFormScreen}
          />
          <Stack.Screen
            name="Perfil"
            options={{
              headerTitle: 'Perfil',
            }}
            component={PerfilScreen}
          />
          <Stack.Screen
            name="ReservasAlumno"
            options={{
              headerTitle: 'Historial',
            }}
            component={ReservasAlumnoScreen}
          />
          <Stack.Screen
            name="Atencion"
            options={{
              headerTitle: 'Atención',
            }}
            component={AtencionScreen}
          />
          <Stack.Screen
            name="AeronavesForm"
            options={{
              headerTitle: 'Aeronave',
            }}
            component={AeronavesFormScreen}
          />
          <Stack.Screen
            name="AlumnosListado"
            options={{
              headerStyle: {
                borderBottomWidth: 0,
                ...elevationShadowStyle(0),
                backgroundColor: colors.primary,
              },
              headerTitle: 'Alumnos',
            }}
            component={AlumnosListadoScreen}
          />
          <Stack.Screen
            name="NotificacionesListado"
            options={{
              headerTitle: 'Notificaciones',
            }}
            component={NotificacionesScreen}
          />
          <Stack.Screen
            name="InstructoresListado"
            options={{
              headerTitle: 'Instructores',
            }}
            component={InstructoresListadoScreen}
          />
          <Stack.Screen
            name="InstructoresHorario"
            options={{
              headerTitle: 'Horario',
            }}
            component={InstructorHorarioScreen}
          />
          <Stack.Screen
            name="InstructoresDetalle"
            options={{
              headerTitle: 'Instructor',
              headerStyle: {
                borderBottomWidth: 0,
                ...elevationShadowStyle(0),
                backgroundColor: colors.primary,
              },
            }}
            component={InstructoresDetalleScreen}
          />
          <Stack.Screen
            name="AprobarReservas"
            options={{
              headerTitle: 'Aprobar Reservas',
            }}
            component={AprobarReservasScreen}
          />
          <Stack.Screen
            name="DetalleReserva"
            options={{
              headerTitle: 'Reserva',
            }}
            component={DetalleReservaScreen}
          />
          <Stack.Screen
            name="Mantenimientos"
            options={({navigation, route}) => ({
              headerTitle: 'Mantenimientos',
              headerRight: () => {
                console.log(route);
                return (
                  <Button
                    type={'clear'}
                    icon={{
                      name: 'ios-cog',
                      type: 'ionicon',
                      color: colors.white,
                    }}
                    onPress={() =>
                      navigation.navigate('CrearMantenimiento', {
                        refresh: route?.params?.refresh,
                      })
                    }
                  />
                );
              },
            })}
            component={MantenimientosListadoScreen}
          />
          <Stack.Screen
            name="CrearMantenimiento"
            options={{
              headerTitle: 'Agendar Mantenimiento',
            }}
            component={CrearMantenimientoScreen}
          />
          <Stack.Screen
            name="PistasListado"
            options={() => ({
              headerTitle: 'Pistas',
            })}
            component={PistasListadoScreen}
          />
          <Stack.Screen
            name="AgendarCierrePista"
            options={{
              headerTitle: 'Agendar Cierre',
            }}
            component={AgendarCierrePistaScreen}
          />
          <Stack.Screen
            name="ReportesListado"
            options={({navigation, route}) => ({
              headerTitle: 'Reportes',
              headerStyle: route.params?.idAlumno
                ? {}
                : {
                    borderBottomWidth: 0,
                    ...elevationShadowStyle(0),
                    backgroundColor: colors.primary,
                  },
              headerRight: () => {
                if (route.params?.idAlumno) {
                  return {};
                }
                return (
                  <Button
                    type={'clear'}
                    icon={{
                      name: 'ios-add',
                      type: 'ionicon',
                      color: colors.white,
                      size: 30,
                    }}
                    onPress={() =>
                      navigation.navigate('TiposReporteListado', {
                        refresh: route?.params?.refresh,
                      })
                    }
                  />
                );
              },
            })}
            component={ReportesListadoScreen}
          />
          <Stack.Screen
            name="TiposReporteListado"
            options={{
              headerTitle: 'Formularios',
            }}
            component={TiposReportesListadoScreen}
          />
          <Stack.Screen
            name="CargarReporte"
            options={{
              headerTitle: 'Cargar Reporte',
            }}
            component={CargarReporteScreen}
          />
          <Stack.Screen
            name="CrearReserva"
            options={{
              headerTitle: 'Crear Reserva',
            }}
            component={CrearReservaScreen}
          />
          <Stack.Screen
            name="EditarAlumno"
            options={{
              headerTitle: 'Editar Alumno',
            }}
            component={EditarAlumnoScreen}
          />
          <Stack.Screen
            name="Notificaciones"
            options={{
              headerTitle: 'Notificaciones',
            }}
            component={NotificacionesScreen}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </UserContext.Provider>
  );
};

export default App;
