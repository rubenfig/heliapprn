import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './Event.styles';

const Event = ({event, onPress, style}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress(event)}
      style={[styles.item, style]}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View
          style={{width: 5, height: '100%', backgroundColor: event.color}}
        />
        <View
          style={{
            flex: 1,
            paddingVertical: 8,
            paddingHorizontal: 2,
          }}>
          <Text style={styles.description}>{event.description}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const eventPropTypes = PropTypes.shape({
  color: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  description: PropTypes.string,
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
});

Event.propTypes = {
  event: eventPropTypes.isRequired,
  onPress: PropTypes.func,
  style: PropTypes.object,
};

export default Event;
