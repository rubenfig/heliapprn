import { StyleSheet } from 'react-native';
import {elevationShadowStyle, perfectSize} from '../../../../src/utils/styling';

const styles = StyleSheet.create({
  item: {
    overflow: 'hidden',
    backgroundColor: '#fff',
    ...elevationShadowStyle(6),
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 12,
    flex: 1,
  },
  description: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(10),
    lineHeight: perfectSize(10),
  },
});

export default styles;
