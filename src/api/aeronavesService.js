import {apiClient} from './api.utils';
const path = 'aeronaves';

const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const listarTodos = async () => {
  return await apiClient.get(`${path}/todos`);
};

const listarMantenimientos = async () => {
  return await apiClient.get(`${path}/listaMantenimento/0`);
};

const agendarMantenimiento = async mantenimiento => {
  return await apiClient.post(`mantenimiento/nuevo`, mantenimiento);
};

const modificarMantenimiento = async mantenimiento => {
  return await apiClient.post(`mantenimiento/modificar`, mantenimiento);
};

const crearAeronave = async aeronave => {
  return await apiClient.post(`${path}/addAeronave`, aeronave);
};

const modificarAeronave = async aeronave => {
  return await apiClient.post(`${path}/modificarAeronave`, aeronave);
};

const listarPistas = async () => {
  return await apiClient.get(`pista/getPistas`);
};

const agendarCierrePista = async cierrePista => {
  return await apiClient.post(`pista/cierrePista`, cierrePista);
};

const confirmarCierrePista = async cierrePista => {
  return await apiClient.post(`pista/confirmarCierrePista`, cierrePista);
};

export const AeronavesService = {
  listar,
  listarMantenimientos,
  listarPistas,
  listarTodos,
  agendarCierrePista,
  agendarMantenimiento,
  confirmarCierrePista,
  crearAeronave,
  modificarAeronave,
  modificarMantenimiento,
};
