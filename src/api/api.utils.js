import axios from 'axios';

// export const url =
//   'http://www.servivuelospy.ml:23080/ServiVuelos-web/webresources';
// export const urlDownload = 'http://www.servivuelospy.ml:23080/ServiVuelos-web';
export const url =
  'http://ec2-18-190-26-74.us-east-2.compute.amazonaws.com:23080/ServiVuelos-web/webresources';
export const urlDownload =
  'http://ec2-18-190-26-74.us-east-2.compute.amazonaws.com:23080/ServiVuelos-web';

const apiClient = axios.create({
  baseURL: url,
  timeout: 5000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});
const apiDownloadClient = axios.create({
  baseURL: urlDownload,
  headers: {
    Accept: 'application/json',
  },
});
export {apiClient, apiDownloadClient};
