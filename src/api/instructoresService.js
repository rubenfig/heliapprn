import {apiClient} from './api.utils';

const path = 'instructores';
const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const cargarHorario = async (horario, limpiar) => {
  return await apiClient.post(
    limpiar ? `${path}/limpiarDiasInstructor` : `${path}/cargarHorario`,
    horario,
  );
};

export const InstructoresService = {
  listar,
  cargarHorario,
};
