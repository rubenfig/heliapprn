import {apiClient} from './api.utils';

const path = 'notificaciones';
const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const listarNotificaciones = async usuario => {
  return await apiClient.get(`${path}/listar/${usuario}`);
};

export const NotificacionesService = {
  listar,
  listarNotificaciones,
};
