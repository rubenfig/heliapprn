import {apiClient, urlDownload} from './api.utils';
const path = 'reportes';

const listar = async () => {
  return await apiClient.get(`${path}/listar`);
};

const listarFormularios = async () => {
  return await apiClient.get(`${path}/listarTiposReportes`);
};

const listarParametros = async id => {
  return await apiClient.get(
    `${path}/listarParametrosReportes?idReporteTipo=${id}`,
  );
};

const listarReportesReserva = async id => {
  return await apiClient.get(`${path}/listarReportesByReserva?idReserva=${id}`);
};

const listarReportesAlumno = async id => {
  return await apiClient.get(`${path}/listarReportesByALumno?idAlumno=${id}`);
};

const descargarReporte = id => {
  return `${urlDownload}/descargarReportesPdf?idReporte=${id}`;
};

const cargarReporte = async reporte => {
  return await apiClient.post(`${path}/cargarReporte`, reporte);
};

export const ReportesService = {
  listar,
  listarFormularios,
  listarParametros,
  listarReportesAlumno,
  listarReportesReserva,
  descargarReporte,
  cargarReporte,
};
