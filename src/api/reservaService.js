import {apiClient} from './api.utils';
const path = 'reserva';
const listarReservasAvion = async (avion, desde, hasta) => {
  return await apiClient.get(
    `${path}/listar/aeronave/${avion}/?fechaInicio=${desde}&fechaFin=${hasta}`,
  );
};

const listarReservasAlumno = async (alumno, desde, hasta) => {
  return await apiClient.get(
    `${path}/listar/alumno/${alumno}/?fechaInicio=${desde}&fechaFin=${hasta}`,
  );
};

const listarReservasInstructor = async (usuario, desde, hasta) => {
  return await apiClient.get(
    `instructores/listarReservasInstructorByUser?idUsuario=${usuario}&fechaInicio=${desde}&fechaFin=${hasta}`,
  );
};

const listarTodasReservasAlumno = async alumno => {
  return await apiClient.get(`${path}/listar/alumno/${alumno}`);
};

const obtenerReserva = async reserva => {
  return await apiClient.get(`${path}/listar/reserva/${reserva}`);
};

const listarReservas = async (desde, hasta) => {
  return await apiClient.get(
    `${path}/listar/reserva/0/?fechaInicio=${desde}&fechaFin=${hasta}`,
  );
};

const listarReservasPendientes = async () => {
  return await apiClient.get(`${path}/listar/pendientes`);
};

const crearReserva = async reserva => {
  return await apiClient.post(`${path}/reservar`, reserva);
};

const modificarEstado = async reserva => {
  return await apiClient.post(`${path}/aprobar`, reserva);
};

const calificarReserva = async calificacion => {
  return await apiClient.post('calificacion/calificar', calificacion);
};

const listarRechazo = async () => {
  return await apiClient.get('tiporechazo/listar');
};

const listarPosiciones = async idReserva => {
  return await apiClient.get(
    'spiderTracks/listarPosiciones?idReserva=' + idReserva,
  );
};

export const ReservaService = {
  listarReservas,
  listarReservasAlumno,
  listarReservasAvion,
  listarReservasInstructor,
  listarRechazo,
  listarPosiciones,
  listarReservasPendientes,
  listarTodasReservasAlumno,
  obtenerReserva,
  modificarEstado,
  crearReserva,
  calificarReserva,
};
