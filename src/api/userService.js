import {apiClient} from './api.utils';
import AsyncStorage from '@react-native-community/async-storage';
import {menuPerfil} from '../utils/utils';

/**
 * Send a POST request to our login endpoint with the data
 * the user entered on the form.
 */
const login = async accountInfo => {
  console.log(accountInfo);
  const res = await apiClient.post('sesion', accountInfo);
  const user = res.data;
  // If the API returned a successful response, mark the user as logged in
  if (user.status === 0 && !user.primerPassword) {
    user.pass = accountInfo.pass;
    const accionesActuales = [];
    if (user) {
      for (const accion of user.acciones.filter(
        a =>
          a !== 'reservas' &&
          a !== 'perfil' &&
          a !== 'configuraciones' &&
          a !== 'notificaciones',
      )) {
        if (menuPerfil.hasOwnProperty(accion)) {
          accionesActuales.push(menuPerfil[accion]);
        }
      }
      if (user.perfil === 'Taller' && accionesActuales.length === 0) {
        accionesActuales.push(menuPerfil.aeronaves_0_hs);
        accionesActuales.push(menuPerfil.pistas);
      }
      if (user.perfil === 'Secretaria') {
        accionesActuales.push(menuPerfil.pistas);
        accionesActuales.push(menuPerfil.instructores);
      }
    }
    user.accionesActuales = accionesActuales;
    await AsyncStorage.setItem('user', JSON.stringify(user));
    return user;
  } else {
    return {error: true, status: -1};
  }
};

const habilitarUsuario = async data => {
  return await apiClient.post('usuarios/habilitarUsuario', data);
};

const resetearPassword = async (data: any) => {
  return await apiClient.post('usuarios/resetearPass', data);
};

const cambiarPassword = async (data: any) => {
  return await apiClient.post('usuarios/resetearPassUsuario', data);
};
const obtenerContacto = async () => {
  return await apiClient.get('util/contacto');
};

const listarUsuarios = async usuario => {
  const filtro = usuario && usuario.length ? `/?usuario=${usuario}` : '';
  return await apiClient.get('usuarios/listar/0' + filtro);
};

const crearUsuario = async usuario => {
  return await apiClient.post('usuarios/addUsuario', usuario);
};

const modificarUsuario = async usuario => {
  return await apiClient.post('usuarios/modificarUsuario', usuario);
};

const listarPerfiles = async () => {
  return await apiClient.get('perfiles/listar/0');
};

const logout = async () => {
  const res = await apiClient.get('sesion/cerrar', null);
  if (res?.data?.estado === 0) {
    await AsyncStorage.removeItem('user');
    // this.touch.delete('HELIAPP');
  }
  return res.data;
};

const getUser = async () => {
  return JSON.parse(await AsyncStorage.getItem('user'));
};

const setRecordado = async recordado => {
  await AsyncStorage.setItem('recordado', recordado.toString());
};

const isRecordado = async () => {
  return (await AsyncStorage.getItem('recordado')) === 'true';
};

export const UserService = {
  login,
  isRecordado,
  setRecordado,
  getUser,
  listarUsuarios,
  logout,
  habilitarUsuario,
  resetearPassword,
  obtenerContacto,
  listarPerfiles,
  cambiarPassword,
  modificarUsuario,
  crearUsuario,
};
