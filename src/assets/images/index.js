export const Images = {
  avatar: require('./avatar.png'),
  lock: require('./lock.png'),
  face: require('./face.png'),
  fingerprint: require('./fingerprint.png'),
  settings: require('./settings.png'),
  calendar: require('./calendar.png'),
  activity: require('./activity.png'),
  home: require('./home.png'),
  user: require('./user.png'),
  chevronUp: require('./chevrop-up.png'),
  chevronDown: require('./chevron-down.png'),
};
