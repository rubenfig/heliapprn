import {ActivityIndicator, Text, TouchableOpacity} from 'react-native';
import {colors, elevationShadowStyle, perfectSize} from '../utils/styling';
import React from 'react';

export const Button = ({title, margin, onPress, loading, containerStyle}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          paddingVertical: 12,
          alignItems: 'center',
          backgroundColor: colors.primary,
          borderRadius: 25,
          ...elevationShadowStyle(6),
        },
        margin && {
          marginVertical: 25,
        },
        containerStyle,
      ]}>
      {loading ? (
        <ActivityIndicator color={colors.white} />
      ) : (
        <Text
          style={{
            fontFamily: 'Kanit-Regular',
            fontSize: perfectSize(16),
            lineHeight: perfectSize(24),
            textAlign: 'center',
            textTransform: 'uppercase',
            color: '#fff',
          }}>
          {title}
        </Text>
      )}
    </TouchableOpacity>
  );
};
