import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {colors, perfectSize} from '../utils/styling';
const styles = StyleSheet.create({
  subtitle: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(13),
    lineHeight: perfectSize(13),
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  body: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(13),
    lineHeight: perfectSize(13),
    color: colors.gray,
  },
});
const DetailRow = ({label, values}) => {
  return (
    <View style={{flexDirection: 'row', marginVertical: 4}}>
      <View style={{flex: 2}}>
        <Text style={styles.subtitle}>{label}</Text>
      </View>
      <View style={{flex: 3}}>
        {values.map(value => (
          <Text style={styles.body}>{value}</Text>
        ))}
      </View>
    </View>
  );
};

export default DetailRow;
