import React from 'react';
import {View} from 'react-native';

export const HR = () => {
  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 16,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(168, 182, 200, 0.211192)',
      }}
    />
  );
};
