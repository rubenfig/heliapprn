import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {perfectSize} from '../utils/styling';
import React from 'react';

export const Input = ({
  label,
  placeholder,
  value,
  onChange,
  onPress,
  containerStyle,
  ...inputProps
}) => {
  return (
    <View
      style={[
        {
          marginTop: 40,
        },
        containerStyle,
      ]}>
      <Text
        style={{
          color: '#666666',
          fontFamily: 'Questrial-Regular',
          fontSize: perfectSize(18),
          lineHeight: perfectSize(23),
        }}>
        {label}
      </Text>
      {onPress ? (
        <TouchableOpacity onPress={onPress}>
          <Text
            style={{
              paddingVertical: 0,
              borderBottomWidth: 1,
              borderBottomColor: '#E6E6E6',
              fontFamily: 'Questrial-Regular',
              fontSize: perfectSize(18),
              lineHeight: perfectSize(23),
            }}>
            {value}
          </Text>
        </TouchableOpacity>
      ) : (
        <TextInput
          value={value}
          onChangeText={onChange}
          style={{
            paddingVertical: 0,
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            fontFamily: 'Questrial-Regular',
            fontSize: perfectSize(18),
            lineHeight: perfectSize(23),
          }}
          placeholderTextColor={'#6C6C6C'}
          placeholder={placeholder}
          {...inputProps}
        />
      )}
    </View>
  );
};
