import React, {Component} from 'react';
import Accordion from 'react-native-collapsible/Accordion';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {colors, elevationShadowStyle, perfectSize} from '../utils/styling';
import {Images} from '../assets/images';

export const styles = StyleSheet.create({
  buttonText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(14),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: colors.white,
    textTransform: 'uppercase',
  },
  button: {
    flex: 1,
    height: perfectSize(50),
    justifyContent: 'center',
  },
  buttonContainer: {
    marginHorizontal: 16,
    flexDirection: 'row',
    backgroundColor: colors.secondary,
    ...elevationShadowStyle(5),
  },
  titleStyle: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(23),
    color: colors.primary,
  },
  subtitleStyle: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(12),
    lineHeight: perfectSize(17),
    color: colors.gray,
  },
});
export class ItemList extends Component {
  state = {
    activeSections: [],
  };

  reset = () => {
    this.setState({activeSections: []});
  };

  _renderContent = item => {
    const {
      leftButton,
      rightButton,
      onLeftPress,
      onRightPress,
      renderMoreInfo,
    } = this.props;
    return (
      <View>
        <View
          style={{
            padding: 16,
            paddingTop: 0,
            paddingBottom: renderMoreInfo ? 16 : 0,
            paddingLeft: 32,
            backgroundColor: '#fff',
            ...elevationShadowStyle(2),
          }}>
          {renderMoreInfo && renderMoreInfo(item)}
        </View>
        <View style={styles.buttonContainer}>
          {leftButton && (
            <TouchableOpacity
              style={styles.button}
              onPress={() => onLeftPress(item)}>
              <Text style={styles.buttonText}>{leftButton}</Text>
            </TouchableOpacity>
          )}
          {leftButton && rightButton && (
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.1)',
                width: 1,
                height: perfectSize(50),
              }}
            />
          )}
          {rightButton && (
            <TouchableOpacity
              style={styles.button}
              onPress={() => onRightPress(item)}>
              <Text style={styles.buttonText}>{rightButton}</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };
  _renderHeader = (item, index) => {
    const {renderMoreInfo, rightButton, leftButton} = this.props;
    const open = this.state.activeSections.includes(index);
    return (
      <View
        style={{
          padding: 16,
          paddingBottom: renderMoreInfo && open ? 0 : 16,
          backgroundColor: '#fff',
          flexDirection: 'row',
          alignItems: 'center',
          ...elevationShadowStyle(2),
        }}>
        {item.image && (
          <Image
            style={{
              width: perfectSize(68),
              height: perfectSize(68),
              borderRadius: perfectSize(68),
            }}
            source={{uri: item.image}}
          />
        )}
        <View style={{flex: 1, marginHorizontal: 16}}>
          <Text style={styles.titleStyle}>{item.title}</Text>
          <Text style={styles.subtitleStyle}>{item.subtitle}</Text>
        </View>
        {leftButton && rightButton && (
          <Image
            resizeMode={'contain'}
            style={{
              width: perfectSize(15),
              height: perfectSize(15),
            }}
            source={open ? Images.chevronUp : Images.chevronDown}
          />
        )}
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({activeSections});
  };

  render() {
    const {items, leftButton, rightButton} = this.props;
    return (
      <ScrollView>
        <Accordion
          containerStyle={{padding: 8}}
          sectionContainerStyle={{marginVertical: 4}}
          sections={items}
          activeSections={this.state.activeSections}
          disabled={!leftButton && !rightButton}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
          onChange={this._updateSections}
        />
      </ScrollView>
    );
  }
}
