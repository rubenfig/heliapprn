import {Text, TouchableOpacity, View} from 'react-native';
import {perfectSize} from '../utils/styling';
import NumericTextInput from '@wwdrew/react-native-numeric-textinput/lib/NumericTextInput';
import React from 'react';

export const NumInput = ({
  label,
  placeholder,
  value,
  onChange,
  onPress,
  ...inputProps
}) => {
  return (
    <View
      style={{
        marginTop: 40,
      }}>
      <Text
        style={{
          color: '#666666',
          fontFamily: 'Questrial-Regular',
          fontSize: perfectSize(18),
          lineHeight: perfectSize(23),
        }}>
        {label}
      </Text>
      {onPress ? (
        <TouchableOpacity onPress={onPress}>
          <Text
            style={{
              paddingVertical: 0,
              borderBottomWidth: 1,
              borderBottomColor: '#E6E6E6',
              fontFamily: 'Questrial-Regular',
              fontSize: perfectSize(18),
              lineHeight: perfectSize(23),
            }}>
            {value}
          </Text>
        </TouchableOpacity>
      ) : (
        <NumericTextInput
          value={value}
          locale={'es-ES'}
          onUpdate={onChange}
          style={{
            paddingVertical: 0,
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            fontFamily: 'Questrial-Regular',
            fontSize: perfectSize(18),
            lineHeight: perfectSize(23),
          }}
          placeholderTextColor={'#6C6C6C'}
          placeholder={placeholder}
          {...inputProps}
        />
      )}
    </View>
  );
};
