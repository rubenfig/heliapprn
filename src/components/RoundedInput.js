import {Image, TextInput, View} from 'react-native';
import {perfectSize} from '../utils/styling';
import React from 'react';

export const RoundedInput = ({placeholder, value, onChange, icon, ...otherProps}) => {
  return (
    <View
      style={{
        marginTop: 20,
        paddingHorizontal: 24,
        borderWidth: 1,
        borderColor: 'rgba(134, 134, 134, 0.19568)',
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
      }}>
      <Image
        source={icon}
        style={{height: perfectSize(13), width: perfectSize(10)}}
        resizeMode="contain"
      />
      <TextInput
        value={value}
        onChangeText={onChange}
        style={{
          flex: 1,
          marginLeft: perfectSize(25),
          fontFamily: 'Questrial-Regular',
          fontSize: perfectSize(20),
          lineHeight: perfectSize(23),
        }}
        placeholderTextColor={'#6C6C6C'}
        placeholder={placeholder}
        {...otherProps}
      />
    </View>
  );
};
