import {Text, View} from 'react-native';
import {perfectSize} from '../utils/styling';
import RNPickerSelect from 'react-native-picker-select';
import React from 'react';

const Select = ({
  onValueChange,
  value,
  placeholder,
  label,
  items,
  disabled,
}) => {
  return (
    <View
      style={{
        marginTop: 40,
      }}>
      <Text
        style={{
          color: '#666666',
          fontFamily: 'Questrial-Regular',
          fontSize: perfectSize(18),
          lineHeight: perfectSize(23),
        }}>
        {label}
      </Text>
      <RNPickerSelect
        disabled={disabled}
        onValueChange={onValueChange}
        value={value}
        placeholder={{
          label: placeholder,
          key: null,
          value: null,
        }}
        style={{
          inputIOS: {
            paddingVertical: 0,
            paddingRight: 30,
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            fontFamily: 'Questrial-Regular',
            fontSize: perfectSize(18),
            lineHeight: perfectSize(23),
          },
          inputAndroid: {
            paddingVertical: 0,
            paddingRight: 30,
            borderBottomWidth: 1,
            borderBottomColor: '#E6E6E6',
            fontFamily: 'Questrial-Regular',
            fontSize: perfectSize(18),
            lineHeight: perfectSize(23),
          },
        }}
        useNativeAndroidPickerStyle={false}
        items={items}
      />
    </View>
  );
};
export default Select;
