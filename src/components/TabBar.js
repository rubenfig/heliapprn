import {View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {Images} from '../assets/images';
import {perfectSize} from '../utils/styling';

const tabIcons = {
  Dashboard: Images.home,
  Settings: Images.settings,
  Cuenta: Images.user,
  Noticias: Images.activity,
  Calendario: Images.calendar,
};

function TabBar({state, descriptors, navigation}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: perfectSize(65),
        backgroundColor: '#fff',
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];

        const isFocused = state.index === index;
        const color = isFocused ? '#FF672B' : '#2E2E39';

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, justifyContent: 'center'}}>
            <Image
              source={tabIcons[route.name]}
              style={{
                alignSelf: 'center',
                tintColor: color,
                width: 16,
                height: 16,
              }}
              resizeMode="contain"
            />
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                right: 0,
                left: 0,
                height: perfectSize(2),
                backgroundColor: isFocused ? '#FF672B' : 'transparent',
              }}
            />
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default TabBar;
