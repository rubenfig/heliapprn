import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import {AeronavesService} from '../../../api/aeronavesService';
import {NumInput} from '../../../components/NumInput';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const AeronavesFormScreen = ({navigation, route}) => {
  console.log(route.params);
  const [loading, setLoading] = useState(false);
  const [editar] = useState(route.params.editar);
  const [aeronave, setAeronave] = useState({
    idAeronave: editar ? route?.params?.aeronave?.idAeronave : null,
    descripcion: editar ? route?.params?.aeronave?.descripcion : '',
    costoPorPeriodo: editar ? route?.params?.aeronave?.costoPorPeriodo + '' : 0,
    horasVueloDisponible: editar
      ? route?.params?.aeronave?.horasVueloDisponible + ''
      : 0,
  });

  const enviarForm = async () => {
    try {
      if (!aeronave.descripcion || aeronave.descripcion.length === 0) {
        Alert.alert('El modelo es requerido');
        return;
      }
      setLoading(true);
      const newAeronave = {...aeronave};
      console.log(newAeronave);
      let res;
      if (editar) {
        res = await AeronavesService.modificarAeronave(newAeronave);
      } else {
        res = await AeronavesService.crearAeronave(newAeronave);
      }
      if (res?.data?.estado === 0) {
        Alert.alert('Avión cargado exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="modelo"
            value={aeronave.descripcion}
            onChange={text => setAeronave({...aeronave, descripcion: text})}
          />
          <NumInput
            label="costo por periodo"
            type="decimal"
            decimalPlaces={2}
            value={aeronave.costoPorPeriodo}
            onChange={text => setAeronave({...aeronave, costoPorPeriodo: text})}
          />
          <NumInput
            label="horas de vuelo disponibles"
            type="decimal"
            decimalPlaces={0}
            value={aeronave.horasVueloDisponible}
            onChange={text =>
              setAeronave({...aeronave, horasVueloDisponible: text})
            }
          />
          <Button
            title="guardar aeronave"
            margin
            onPress={() => enviarForm()}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AeronavesFormScreen;
