import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  Text,
  Switch,
} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../components/ItemList';
import {UserContext} from '../../utils/UserContext';
import {AeronavesService} from '../../api/aeronavesService';
import {numberFormat} from '../../utils/utils';
import {SearchBar} from 'react-native-elements';
import {colors} from '../../utils/styling';

const AeronavesListadoScreen = ({navigation}) => {
  const {user} = useContext(UserContext);
  const itemList = useRef();
  let perfil;
  if (user && user.perfil) {
    perfil = user.perfil.toLowerCase();
  }
  const [loading, setLoading] = useState(false);
  const [filtro, setFiltro] = useState(null);
  const [aeronaves, setAeronaves] = useState([]);
  const [aeronavesFiltradas, setAeronavesFiltradas] = useState([]);

  useEffect(() => {
    listarAviones();
    navigation.setParams({refresh: listarAviones});
  }, []);

  const filtrar = () => {
    const newAeronavesFiltradas = aeronaves.filter(aeronave => {
      return (
        aeronave.descripcion &&
        aeronave.descripcion.toLowerCase().includes(filtro.toLowerCase())
      );
    });
    setAeronavesFiltradas(newAeronavesFiltradas);
    itemList?.current?.reset();
  };

  const renderMoreInfo = item => {
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>
          Horas de vuelo disponibles:{' '}
          {numberFormat.format(item.horasVueloDisponible)}
        </Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[itemListStyles.subtitleStyle, {flex: 1}]}>
            Habilitado
          </Text>
          <Switch
            value={item.habilitado}
            onValueChange={value => {
              const newAeronaves = aeronaves.map(r => {
                return r.idAeronave === item.idAeronave
                  ? {...r, habilitado: value}
                  : r;
              });
              setAeronaves(newAeronaves);
              habilitarAeronave(item, value);
            }}
          />
        </View>
      </View>
    );
  };

  const habilitarAeronave = async (aeronave, value) => {
    const data = {
      idAeronave: aeronave.idAeronave,
      habilitado: value,
    };
    setLoading(true);
    const response = await AeronavesService.modificarAeronave(data);
    if (response.data && response.data.estado === 0) {
      Alert.alert('Reserva modificada exitosamente!');
      await listarAviones();
    } else {
      Alert.alert(response?.data?.mensaje || 'Ocurrió un error indeterminado');
    }
  };

  const listarAviones = async () => {
    setLoading(true);
    try {
      let listarFunc;
      if (perfil.includes('alumno')) {
        listarFunc = await AeronavesService.listar();
      } else {
        listarFunc = await AeronavesService.listarTodos();
      }
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevasAeronaves = response.lista.map(a => {
          return {
            ...a,
            id: a.idAeronave,
            title: a.descripcion,
            subtitle:
              'Costo por periodo: ' + numberFormat.format(a.costoPorPeriodo),
          };
        });
        setAeronaves(nuevasAeronaves);
        setAeronavesFiltradas(nuevasAeronaves);
        itemList?.current?.reset();
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <SearchBar
        inputContainerStyle={{backgroundColor: colors.white}}
        containerStyle={{
          backgroundColor: colors.primary,
          borderBottomWidth: 0,
          borderTopWidth: 0,
        }}
        placeholder="Buscar..."
        onChangeText={setFiltro}
        onSubmitEditing={filtrar}
        value={filtro}
      />
      <ItemList
        ref={itemList}
        items={aeronavesFiltradas}
        leftButton={'reservar'}
        rightButton={perfil.includes('secretaria') ? 'editar' : null}
        onLeftPress={item => console.log('left', item.id)}
        onRightPress={item =>
          navigation.navigate('AeronavesForm', {
            editar: true,
            aeronave: item,
            refresh: listarAviones,
          })
        }
        renderMoreInfo={perfil.includes('secretaria') ? renderMoreInfo : null}
      />
    </View>
  );
};

export default AeronavesListadoScreen;
