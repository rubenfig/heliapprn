import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import {AlumnosService} from '../../../api/alumnosService';
import Select from '../../../components/Select';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {CursosService} from '../../../api/cursosService';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const EditarAlumnoScreen = ({navigation, route}) => {
  const [showCMAPicker, setShowCMAPicker] = useState(false);
  const [showPermisoPicker, setShowPermisoPicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [curso, setCurso] = useState(null);
  const [cursos, setCursos] = useState([]);
  const [alumno, setAlumno] = useState(route.params.alumno);

  React.useEffect(() => {
    return navigation.addListener('focus', () => {
      console.log('test');
      listarCursos();
    });
  }, [navigation]);

  const listarCursos = async () => {
    setLoading(true);
    try {
      const listarFunc = await CursosService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setCursos(response);
        response.map(c => {
          c.idCurso === alumno.idCurso && setCurso(c);
        });
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const editarAlumno = async () => {
    try {
      setLoading(true);
      const newAlumno = {
        idAlumno: alumno.idAlumno,
        fechaVencimientoPermiso: new Date(alumno.fechaVencimientoPermiso),
        cma: new Date(alumno.cma),
        idCurso: curso.idCurso,
        nroLicenciaVuelo: alumno.nroLicenciaVuelo,
      };
      console.log(JSON.stringify(newAlumno));
      const res = await AlumnosService.modificarAlumno(newAlumno);
      if (res?.data?.estado === 0) {
        Alert.alert('Alumno editado exitosamente!');
        navigation.goBack();
        route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input label="alumno" value={alumno.nombreCompleto} disabled />
          <Input
            label="vencimiento de carnet"
            value={moment(alumno.fechaVencimientoPermiso).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowPermisoPicker(true)}
          />
          <Input
            label="vencimiento CMA"
            value={moment(alumno.cma).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowCMAPicker(true)}
          />
          {showCMAPicker && (
            <DateTimePicker
              value={alumno.cma}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowCMAPicker(false);
                date && setAlumno({...alumno, cma: date});
              }}
            />
          )}
          {showPermisoPicker && (
            <DateTimePicker
              value={alumno.fechaVencimientoPermiso}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowPermisoPicker(false);
                date && setAlumno({...alumno, fechaVencimientoPermiso: date});
              }}
            />
          )}
          <Select
            onValueChange={itemValue => {
              setCurso(itemValue);
            }}
            label={'curso'}
            value={curso}
            placeholder={'Elija una aeronave'}
            items={cursos?.map(a => {
              return {key: a.idCurso, label: a.descripcion, value: a};
            })}
          />
          <Input
            label="licencia de vuelo"
            value={alumno.nroLicenciaVuelo}
            onChange={value => setAlumno({...alumno, nroLicenciaVuelo: value})}
          />
          <Button title="editar alumno" margin onPress={() => editarAlumno()} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default EditarAlumnoScreen;
