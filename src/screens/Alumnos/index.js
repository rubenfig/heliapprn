import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, View, StyleSheet, Alert, Text} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../components/ItemList';
import {AlumnosService} from '../../api/alumnosService';
import {colors} from '../../utils/styling';
import {SearchBar} from 'react-native-elements';
import {numberFormat} from '../../utils/utils';
import moment from 'moment';

const AlumnosListadoScreen = ({navigation}) => {
  const itemList = useRef();
  const [filtro, setFiltro] = useState(null);
  const [loading, setLoading] = useState(false);
  const [alumnos, setAlumnos] = useState([]);
  const [alumnosFiltrados, setAlumnosFiltrados] = useState([]);

  useEffect(() => {
    listarAlumnos();
  }, []);

  const filtrar = () => {
    const newAlumnos = alumnos.filter(alumno => {
      return (
        alumno.nombreCompleto &&
        alumno?.nombreCompleto?.toLowerCase().includes(filtro.toLowerCase())
      );
    });
    setAlumnosFiltrados(newAlumnos);
    itemList?.current?.reset();
  };
  const listarAlumnos = async () => {
    setLoading(true);
    try {
      const listarFunc = await AlumnosService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevosAlumnos = response.lista.map(a => {
          return {
            ...a,
            id: a.idAlumno,
            title: a.nombreCompleto,
            subtitle: 'Documento: ' + a.documento,
          };
        });
        setAlumnos(nuevosAlumnos);
        setAlumnosFiltrados(nuevosAlumnos);
        itemList?.current?.reset();
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const renderMoreInfo = item => {
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>Correo: {item.correo}</Text>
        <Text style={itemListStyles.subtitleStyle}>
          Horas de vuelo: {numberFormat.format(item.horasVuelo)}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Curso: {item.cursoDescripcion}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Direccion: {item.direccion}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Licencia de vuelo: {item.nroLicenciaVuelo}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Vencimiento carnet:{' '}
          {moment(item.fechaVencimientoPermiso).format('DD-MM-YYYY')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Vencimiento CMA: {moment(item.cma).format('DD-MM-YYYY')}
        </Text>
      </View>
    );
  };
  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <SearchBar
        inputContainerStyle={{backgroundColor: colors.white}}
        containerStyle={{
          backgroundColor: colors.primary,
          borderBottomWidth: 0,
          borderTopWidth: 0,
        }}
        placeholder="Buscar..."
        onChangeText={setFiltro}
        onSubmitEditing={filtrar}
        value={filtro}
      />
      <ItemList
        ref={itemList}
        items={alumnosFiltrados}
        leftButton={'ver reportes'}
        rightButton={'editar'}
        renderMoreInfo={renderMoreInfo}
        onLeftPress={item => console.log('left', item.id)}
        onRightPress={item =>
          navigation.navigate('EditarAlumno', {
            alumno: item,
            refresh: listarAlumnos,
          })
        }
      />
    </View>
  );
};

export default AlumnosListadoScreen;
