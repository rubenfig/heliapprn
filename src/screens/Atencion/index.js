import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
  Alert,
  Linking,
} from 'react-native';
import {colors, elevationShadowStyle, perfectSize} from '../../utils/styling';
import DetailRow from '../../components/DetailRow';
import {Button as ElementsButton} from 'react-native-elements';
import {UserService} from '../../api/userService';
const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    paddingVertical: 16,
    paddingHorizontal: 20,
    marginHorizontal: 20,
    marginTop: 32,
    ...elevationShadowStyle(4),
  },
  title: {
    marginVertical: 16,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(26),
    lineHeight: perfectSize(32),
    textAlign: 'center',
    color: colors.primary,
    textTransform: 'uppercase',
  },
  button: {
    backgroundColor: colors.secondary,
    borderRadius: 100,
    height: 40,
    width: 40,
    padding: 0,
  },
});
const AtencionScreen = () => {
  const [loading, setLoading] = useState(false);
  const [atencion, setAtencion] = useState({});
  useEffect(() => {
    obtenerAtencion();
  }, []);
  const obtenerAtencion = async () => {
    try {
      setLoading(true);
      const response = await UserService.obtenerContacto();
      console.log(response);
      const newAtencion = {};
      for (const contacto of response?.data?.lista) {
        switch (contacto.codigo) {
          case 'CONTACTO_CORREO':
            newAtencion.correo = contacto.valor;
            break;
          case 'CONTACTO_NUMERO':
            newAtencion.telefono = contacto.valor;
            break;
          case 'CONTACTO_DATOS':
            newAtencion.nombre = contacto.valor;
            break;
        }
      }
      setAtencion(newAtencion);
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <View style={styles.card}>
        <Text style={styles.title}>{atencion?.nombre}</Text>
        <DetailRow label={'correo'} values={[atencion?.correo]} />
        <DetailRow label={'telefono'} values={[atencion?.telefono]} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginVertical: 50,
          }}>
          <ElementsButton
            onPress={() => {
              Linking.openURL('mailto:' + atencion.correo);
            }}
            type={'solid'}
            buttonStyle={styles.button}
            icon={{name: 'ios-mail', type: 'ionicon', color: colors.white}}
          />
          <ElementsButton
            onPress={() => {
              Linking.openURL('tel:' + atencion.telefono);
            }}
            type={'solid'}
            buttonStyle={styles.button}
            icon={{name: 'ios-call', type: 'ionicon', color: colors.white}}
          />
          <ElementsButton
            onPress={() => {
              Linking.openURL(
                'https://api.whatsapp.com/send?phone=' + atencion.telefono,
              );
            }}
            type={'solid'}
            buttonStyle={styles.button}
            icon={{name: 'logo-whatsapp', type: 'ionicon', color: colors.white}}
          />
        </View>
      </View>
    </View>
  );
};

export default AtencionScreen;
