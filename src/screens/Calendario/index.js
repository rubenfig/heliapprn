import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  StatusBar,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  CalendarProvider,
  LocaleConfig,
} from '../../../local_modules/react-native-calendars';
import {colors, perfectSize} from '../../utils/styling';
import ExpandableCalendar from '../../../local_modules/react-native-calendars/src/expandableCalendar';
import WeekView from '../../../local_modules/react-native-week-view';
import moment from 'moment';
import {UserContext} from '../../utils/UserContext';
import {ReservaService} from '../../api/reservaService';
import {reservaEstados} from '../../utils/utils';
import Entypo from 'react-native-vector-icons/Entypo';

LocaleConfig.locales.es = {
  monthNames: [
    'ENERO',
    'FEBRERO',
    'MARZO',
    'ABRIL',
    'MAYO',
    'JUNIO',
    'JULIO',
    'AGOSTO',
    'SEPTIEMBRE',
    'OCTUBRE',
    'NOVIEMBRE',
    'DICIEMBRE',
  ],
  monthNamesShort: [
    'Ene.',
    'Feb.',
    'Mar.',
    'Abr.',
    'Mayo',
    'Jun.',
    'Jul.',
    'Agos.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miércoles',
    'Jueves',
    'Viernes',
    'Sábado',
  ],
  dayNamesShort: ['DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB'],
  today: 'Hoy',
};
LocaleConfig.defaultLocale = 'es';

const theme = {
  backgroundColor: colors.secondary,
  calendarBackground: colors.secondary,
  textSectionTitleColor: colors.white,
  selectedDayBackgroundColor: colors.tint2,
  selectedDayTextColor: colors.white,
  todayTextColor: colors.white,
  dayTextColor: colors.white,
  textDisabledColor: 'rgba(255,255,255,0.2)',
  arrowColor: colors.white,
  monthTextColor: colors.white,
  textDayFontFamily: 'Inter',
  textMonthFontFamily: 'Inter',
  textDayHeaderFontFamily: 'Inter',
  textDayFontWeight: '500',
  textMonthFontWeight: '500',
  textDayHeaderFontWeight: '500',
  textDayFontSize: perfectSize(20),
  textMonthFontSize: perfectSize(20),
  textDayHeaderFontSize: perfectSize(12),
};
const CalendarioScreen = ({navigation}) => {
  const {user} = useContext(UserContext);
  const [currentHour, setCurrentHour] = useState(moment().hour());
  const [nextDisabled, setNextDisabled] = useState(false);
  const [events, setEvents] = useState([]);
  const [maxDate] = useState(moment().add(2, 'months'));
  const [perfil] = useState(user?.perfil?.toLowerCase());
  const [loading, setLoading] = useState(false);
  const [idUsuario] = useState(perfil && user?.usuario?.idUsuario);
  const [idAlumno] = useState(
    perfil?.includes('alumno') && user?.alumno?.idAlumno,
  );
  const [originalDate] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(moment());
  const weekStart = moment(selectedDate)
    .startOf('isoWeek')
    .toDate();
  useEffect(() => {
    listarReservas();
  }, [selectedDate]);

  const listarReservas = async () => {
    try {
      console.log('listando');
      console.log(selectedDate);
      if (perfil) {
        setLoading(true);
        const desde = moment(weekStart)
          .hour(0)
          .minutes(0)
          .seconds(0)
          .format('YYYY-MM-DD HH:mm:ss');
        const hasta = moment(weekStart)
          .hour(0)
          .minutes(0)
          .seconds(0)
          .add(6, 'd')
          .format('YYYY-MM-DD HH:mm:ss');
        let res;
        if (perfil && perfil.includes('alumno')) {
          res = await ReservaService.listarReservasAlumno(
            idAlumno,
            desde,
            hasta,
          );
        } else if (perfil && perfil.includes('instructor')) {
          res = await ReservaService.listarReservasInstructor(
            idUsuario,
            desde,
            hasta,
          );
        } else {
          res = await ReservaService.listarReservas(desde, hasta);
        }
        setCurrentHour(moment().hour());
        setNextDisabled(
          maxDate.isBefore(moment(selectedDate).add(4, 'd')) &&
            perfil.includes('alumno'),
        );
        if (res.data && res.data.estado === 0) {
          const events = [];
          for (const reserva of res.data.lista) {
            const event = {
              color: null,
              id: reserva.idReserva,
              reserva,
              startDate: moment(reserva.inicio).toDate(),
              endDate: moment(reserva.fin).toDate(),
            };
            for (const estado of reservaEstados) {
              if (estado.idEstadoReserva === reserva.estadoReserva) {
                event.description = estado.descripcion;
                event.color = estado.color.primary;
                reserva.estado = estado;
              }
            }
            events.push(event);
          }
          // console.log(events);
          setEvents([...events]);
        }
      }
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 3}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.secondary} />
      <CalendarProvider date={originalDate} key={'1'} theme={theme}>
        <ExpandableCalendar
          disableArrowRight={nextDisabled}
          staticHeader={false}
          scrollEnabled={false}
          disableWeekScroll={true}
          onDayPress={date =>
            setSelectedDate(moment(date.dateString), 'YYY-MM-DD')
          }
          current={originalDate}
          horizontal={true}
          theme={theme}
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('CrearReserva', {selectedDate})}
          style={{
            height: perfectSize(50),
            width: perfectSize(50),
            borderRadius: perfectSize(50),
            backgroundColor: colors.primary,
            position: 'absolute',
            bottom: 20,
            right: 20,
            zIndex: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Entypo name="plus" color={colors.white} size={perfectSize(30)} />
        </TouchableOpacity>
        <WeekView
          selectedDate={weekStart}
          numberOfDays={5}
          events={events}
          onTimePress={datetime =>
            navigation.navigate('CrearReserva', {
              selectedDate: datetime,
              refresh: listarReservas,
            })
          }
          onEventPress={({reserva}) =>
            navigation.navigate('DetalleReserva', {
              reserva,
              refresh: listarReservas,
            })
          }
        />
      </CalendarProvider>
    </View>
  );
};

export default CalendarioScreen;
