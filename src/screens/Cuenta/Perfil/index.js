import React, {useContext, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import {
  colors,
  elevationShadowStyle,
  perfectSize,
} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import moment from 'moment';
import {UserContext} from '../../../utils/UserContext';
import DetailRow from '../../../components/DetailRow';
import {numberFormat} from '../../../utils/utils';
const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    paddingVertical: 16,
    paddingHorizontal: 20,
    marginHorizontal: 20,
    marginTop: 32,
    ...elevationShadowStyle(4),
  },
  title: {
    marginVertical: 16,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(26),
    lineHeight: perfectSize(32),
    textAlign: 'center',
    color: colors.primary,
    textTransform: 'uppercase',
  },
});
const PerfilScreen = ({}) => {
  const [loading] = useState(false);
  const {user} = useContext(UserContext);
  const perfil = {...user.alumno, nombreCompleto: user.usuario.nombreCompleto};
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <View style={styles.card}>
        <Text style={styles.title}>{perfil?.nombreCompleto}</Text>
        <DetailRow
          label={'Horas de vuelo'}
          values={[numberFormat.format(perfil?.horasVuelo)]}
        />
        <DetailRow
          label={'último vuelo'}
          values={[
            perfil?.ultimoVuelo &&
              moment(perfil.ultimoVuelo).format('DD-MM-YYYY'),
          ]}
        />
        <DetailRow label={'curso'} values={[perfil?.curso?.descripcion]} />
        <DetailRow
          label={'habilitado'}
          values={[perfil.habilitado ? 'Sí' : 'No']}
        />
        <DetailRow
          label={'vencimiento carnet'}
          values={[
            perfil?.fechaVencimientoPermiso &&
              moment(perfil.fechaVencimientoPermiso).format('DD-MM-YYYY'),
          ]}
        />
        <DetailRow
          label={'vencimiento cma'}
          values={[perfil?.cma && moment(perfil.cma).format('DD-MM-YYYY')]}
        />
        <DetailRow
          label={'Horas de vuelo'}
          values={[numberFormat.format(perfil?.horasVuelo)]}
        />
        <DetailRow
          label={'Horas de vuelo'}
          values={[numberFormat.format(perfil?.horasVuelo)]}
        />
        <DetailRow
          label={'Licencia de vuelo'}
          values={[perfil?.nroLicenciaVuelo]}
        />
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 32,
          left: 20,
          right: 20,
        }}>
        <Button
          title={'ver reportes'}
          style={{
            marginHorizontal: 20,
          }}
        />
      </View>
    </View>
  );
};

export default PerfilScreen;
