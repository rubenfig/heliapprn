import React, {useContext, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {colors, elevationShadowStyle, perfectSize} from '../../utils/styling';
import {UserContext} from '../../utils/UserContext';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {UserService} from '../../api/userService';

const CuentaScreen = ({navigation}) => {
  const {user} = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  console.log(user);
  const Option = ({title, onPress}) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          borderRadius: 32,
          padding: 20,
          marginHorizontal: 30,
          backgroundColor: colors.white,
          marginVertical: 10,
          ...elevationShadowStyle(5),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: 'Questrial',
            fontSize: perfectSize(18),
            lineHeight: perfectSize(23),
            color: colors.tint3,
          }}>
          {title}
        </Text>
        <MaterialIcons name={'chevron-right'} size={18} color={colors.tint2} />
      </TouchableOpacity>
    );
  };

  const logout = async () => {
    try {
      setLoading(true);
      const res = await UserService.logout();
      if (res.estado === 0) {
        navigation.popToTop();
      }
    } catch (e) {
      console.log(e);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };
  return (
    <ScrollView style={{backgroundColor: colors.white, flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 1000}]}
          size={'large'}
        />
      )}
      <Text
        style={{
          fontFamily: 'Questrial',
          fontSize: perfectSize(22),
          lineHeight: perfectSize(32),
          color: colors.tint3,
          marginTop: 30,
          marginHorizontal: 30,
          marginBottom: 16,
        }}>
        mi cuenta
      </Text>
      <Option
        title={user?.usuario?.nombreCompleto}
        onPress={() => navigation.navigate('Perfil')}
      />
      <Option title={user?.usuario?.correo} />
      <Option
        title={'contraseña'}
        onPress={() => navigation.navigate('CambiarPassword')}
      />
      <Option
        title={'mis vuelos'}
        onPress={() => navigation.navigate('ReservasAlumno')}
      />
      <Option title={'mi cuota'} />
      <Option
        title={'notificaciones'}
        onPress={() => navigation.navigate('NotificacionesListado')}
      />
      <Option title={'salir'} onPress={() => logout()} />
    </ScrollView>
  );
};

export default CuentaScreen;
