import React, {useContext, useRef} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Drawer from 'react-native-drawer';
import {UserContext} from '../../utils/UserContext';
import {ListItem} from 'react-native-elements';
import {perfectSize} from '../../utils/styling';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {colors} from '../../utils/styling';
const DashboardScreen = ({navigation}) => {
  const {user} = useContext(UserContext);
  const drawer = useRef();
  return (
    <Drawer
      ref={drawer}
      type="overlay"
      content={
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <ScrollView>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                textAlign: 'center',
                textTransform: 'uppercase',
                fontSize: perfectSize(30),
                paddingVertical: 20,
                backgroundColor: colors.primary,
                color: colors.white,
              }}>
              Menú
            </Text>
            {user &&
              user.accionesActuales?.map((accion, index) => (
                <ListItem
                  leftIcon={{name: accion.icon, type: 'ionicon'}}
                  key={accion.title}
                  containerStyle={{
                    paddingVertical: 8,
                  }}
                  titleStyle={{
                    fontFamily: 'Questrial-Regular',
                  }}
                  title={accion.title}
                  onPress={() => {
                    navigation.navigate(accion.url);
                    drawer?.current?.close();
                  }}
                  chevron
                  topDivider
                  bottomDivider
                />
              ))}
          </ScrollView>
        </View>
      }
      openDrawerOffset={0.4}
      tapToClose={true}>
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={{margin: 16}}
          onPress={() => drawer?.current?.open()}>
          <Ionicons name={'ios-menu'} size={perfectSize(35)} />
        </TouchableOpacity>
      </View>
    </Drawer>
  );
};

export default DashboardScreen;
