import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  CheckBox,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {InstructoresService} from '../../../api/instructoresService';
import {Overlay} from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});
const dias = [
  {label: 'Domingo', value: '1'},
  {label: 'Lunes', value: '2'},
  {label: 'Martes', value: '3'},
  {label: 'Miércoles', value: '4'},
  {label: 'Jueves', value: '5'},
  {label: 'Viernes', value: '6'},
  {label: 'Sábado', value: '7'},
];

const InstructorHorarioScreen = ({navigation, route}) => {
  const [showTimePickerIni, setShowTimePickerIni] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [mostrarDias, setMostrarDias] = useState(false);
  const [loading, setLoading] = useState(false);
  const limpiar = route?.params?.limpiar;
  const [horario, setHorario] = useState({
    inicio: new Date(),
    fin: new Date(),
    dias: [],
    idInstructor: route?.params?.instructor?.idInstructor,
  });

  const guardarHorario = async () => {
    try {
      setLoading(true);
      const newHorario = {...horario};
      newHorario.inicio = moment(newHorario.inicio).format('HH:mm:ss');
      newHorario.fin = moment(newHorario.fin).format('HH:mm:ss');
      newHorario.dias = newHorario.dias.map(d => {
        // tslint:disable-next-line:radix
        return parseInt(d);
      });
      console.log(JSON.stringify(newHorario));
      console.log(newHorario);
      const res = await InstructoresService.cargarHorario(newHorario, limpiar);
      if (res?.data?.estado === 0) {
        Alert.alert('Horario modificado exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const DiaCheckbox = ({dia}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginHorizontal: '20%',
        }}>
        <Text style={[styles.infoText, {flex: 1}]}>{dia.label}</Text>
        <CheckBox
          value={horario.dias.includes(dia.value)}
          onChange={value => {
            let newDias = horario.dias;
            if (value) {
              newDias.push(dia.value);
            } else {
              newDias = newDias.filter(d => d !== dia.value);
            }
            setHorario({...horario, dias: newDias});
          }}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <Overlay
        isVisible={mostrarDias}
        overlayStyle={{
          paddingVertical: 32,
          paddingHorizontal: 16,
          height: 'auto',
        }}>
        {dias.map(dia => (
          <DiaCheckbox dia={dia} />
        ))}
        <Button
          title={'Cerrar'}
          containerStyle={{marginTop: 32}}
          onPress={() => setMostrarDias(false)}
        />
      </Overlay>
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="inicio"
            value={moment(horario.inicio).format('HH:mm')}
            disabled
            onPress={() => setShowTimePickerIni(true)}
          />
          <Input
            label="fin"
            value={moment(horario.fin).format('HH:mm')}
            disabled
            onPress={() => setShowTimePicker(true)}
          />
          <Input
            label="dias"
            value={horario.dias
              .sort()
              .map(d => dias[parseInt(d) - 1].label)
              .join(', ')}
            disabled
            onPress={() => setMostrarDias(true)}
          />
          {showTimePickerIni && (
            <DateTimePicker
              value={horario.inicio}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                showTimePickerIni(false);
                date && setHorario({...horario, inicio: date});
              }}
            />
          )}
          {showTimePicker && (
            <DateTimePicker
              value={horario.fin}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                setShowTimePicker(false);
                date && setHorario({...horario, fin: date});
              }}
            />
          )}
          <Button
            disabled={
              (!limpiar && (!horario.inicio || !horario.fin)) ||
              !horario.dias ||
              horario.dias.length === 0
            }
            title={limpiar ? 'cargar horario' : 'limpiar horario'}
            margin
            onPress={() => guardarHorario()}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default InstructorHorarioScreen;
