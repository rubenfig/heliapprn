import React from 'react';
import {Image, Text, View} from 'react-native';
import {
  colors,
  elevationShadowStyle,
  perfectSize,
} from '../../../utils/styling';
import {Images} from '../../../assets/images';
import {Button} from '../../../components/Button';

const InstructoresDetalleScreen = ({route}) => {
  const {instructor} = route.params;
  console.log(instructor);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      <View
        style={{
          backgroundColor: colors.primary,
          height: perfectSize(80),
          ...elevationShadowStyle(5),
        }}
      />
      <View
        style={{
          ...elevationShadowStyle(5),
          overflow: 'hidden',
          alignSelf: 'center',
          height: perfectSize(100),
          width: perfectSize(100),
          borderRadius: perfectSize(100),
          top: -perfectSize(50),
        }}>
        <Image
          source={Images.avatar}
          resizeMode={'contain'}
          style={{
            backgroundColor: colors.white,
            height: perfectSize(100),
            width: perfectSize(100),
          }}
        />
      </View>
      <Text
        style={{
          fontSize: perfectSize(26),
          lineHeight: perfectSize(32),
          textAlign: 'center',
          color: colors.primary,
          textTransform: 'capitalize',
        }}>
        {instructor?.nombreCompleto}
      </Text>
      <View
        style={{
          backgroundColor: '#fff',
          ...elevationShadowStyle(4),
        }}
      />
      <View
        style={{
          position: 'absolute',
          bottom: 32,
          left: 20,
          right: 20,
        }}>
        <Button
          title={'elegir instructor'}
          style={{
            marginHorizontal: 20,
          }}
        />
      </View>
    </View>
  );
};

export default InstructoresDetalleScreen;
