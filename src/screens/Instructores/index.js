import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, View, StyleSheet, Alert} from 'react-native';
import {ItemList} from '../../components/ItemList';
import {InstructoresService} from '../../api/instructoresService';
import {UserContext} from '../../utils/UserContext';

const InstructoresListadoScreen = ({navigation}) => {
  const {user} = useContext(UserContext);
  const perfil = user.perfil.toLowerCase();
  const [loading, setLoading] = useState(false);
  const [instructores, setInstructores] = useState([]);

  useEffect(() => {
    listarInstructores();
  }, []);

  const listarInstructores = async () => {
    setLoading(true);
    try {
      const listarFunc = await InstructoresService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevosInstructores = response.lista.map(a => {
          console.log(a);
          return {
            ...a,
            id: a.idAlumno,
            title: a.nombreCompleto,
            subtitle: a.correo,
          };
        });
        setInstructores(nuevosInstructores);
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <ItemList
        items={instructores}
        leftButton={perfil.includes('secretaria') && 'cargar horario'}
        rightButton={perfil.includes('secretaria') && 'limpiar horario'}
        onLeftPress={item =>
          navigation.navigate('InstructoresHorario', {
            instructor: item,
            limpiar: false,
          })
        }
        onRightPress={item =>
          navigation.navigate('InstructoresHorario', {
            instructor: item,
            limpiar: true,
          })
        }
      />
    </View>
  );
};

export default InstructoresListadoScreen;
