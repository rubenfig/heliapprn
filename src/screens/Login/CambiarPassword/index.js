import React, {useState} from 'react';
import {
  StyleSheet,
  Alert,
  View,
  SafeAreaView,
  ActivityIndicator,
  Text,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Input} from '../../../components/Input';
import {Button} from '../../../components/Button';
import {UserService} from '../../../api/userService';
import {Button as RNEButton} from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
  },
  icon: {
    width: perfectSize(50),
    height: perfectSize(50),
  },
  infoText: {
    marginVertical: 6,
    marginHorizontal: 16,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(12),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#666666',
  },
  iconText: {
    marginTop: 4,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(14),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#000',
  },
  bottomText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
    marginBottom: perfectSize(50),
  },
});

const CambiarPasswordScreen = ({navigation, route}) => {
  const [passwordActual, setPasswordActual] = useState(
    route?.params?.passActual || '',
  );
  const [nuevoPassword, setNuevoPassword] = useState('');
  const [confirmar, setConfirmar] = useState('');
  const [loading, setLoading] = useState(false);
  const cambiarPassword = () => {
    try {
      setLoading(true);
      if (nuevoPassword === confirmar) {
        const resp = UserService.cambiarPassword({
          passActual: passwordActual,
          passNuevo: nuevoPassword,
        });
        if (resp.estado === 0) {
          navigation.goBack();
        } else {
          Alert.alert(resp.mensaje || 'Ocurrió un error indeterminado');
        }
      } else {
        Alert.alert('Las claves no coinciden');
      }
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado');
    } finally {
      setLoading(false);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <View
        style={{
          marginTop: 30,
          marginHorizontal: 16,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <RNEButton
          type={'clear'}
          onPress={navigation.goBack}
          icon={{name: 'chevron-left', color: colors.primary, size: 30}}
        />
        <Text
          style={{
            fontFamily: 'Questrial',
            fontSize: perfectSize(22),
            lineHeight: perfectSize(32),
            color: colors.tint3,
          }}>
          cambiar contraseña
        </Text>
      </View>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="contraseña actual"
            value={passwordActual}
            onChange={setPasswordActual}
            autoCompleteType="password"
            secureTextEntry={true}
          />
          <Input
            label="nueva contraseña"
            value={nuevoPassword}
            onChange={setNuevoPassword}
            autoCompleteType="password"
            secureTextEntry={true}
          />
          <Input
            label="confirmar contraseña"
            value={confirmar}
            onChange={setConfirmar}
            autoCompleteType="password"
            secureTextEntry={true}
          />
          <View style={{height: 30}} />
          <Button title="cambiar contraseña" margin onPress={cambiarPassword} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default CambiarPasswordScreen;
