import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
  },
  icon: {
    width: perfectSize(50),
    height: perfectSize(50),
  },
  infoText: {
    marginVertical: 6,
    marginHorizontal: 16,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(12),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#666666',
  },
  iconText: {
    marginTop: 4,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(14),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#000',
  },
  bottomText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
    marginBottom: perfectSize(50),
  },
});

const RecuperarPasswordScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Text style={styles.header}>Olvidaste tu contraseña</Text>
          <Text style={styles.infoText}>
            ingrese su correo electrónico a continuación para recibir
            instrucciones de restablecimiento de contraseña
          </Text>
          <Input label="email" />
          <View style={{height: 30}} />
          <Button title="recuperar contraseña" margin />
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.bottomText, {color: '#7A7A7A'}]}>
              no tienes cuenta?{' '}
            </Text>
            <TouchableOpacity onPress={() => navigation.popToTop()}>
              <Text style={[styles.bottomText, {color: colors.tint2}]}>
                crear cuenta
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default RecuperarPasswordScreen;
