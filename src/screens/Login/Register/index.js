import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
  },
  icon: {
    width: perfectSize(50),
    height: perfectSize(50),
  },
  iconText: {
    marginTop: 4,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(14),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#000',
  },
  bottomText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
    marginVertical: perfectSize(50),
  },
});

const RegisterScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1, justifyContent: 'flex-end'}}>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Text style={styles.header}>Crear cuenta</Text>
          <Input label="nombre" />
          <Input label="email" />
          <Input label="contraseña" />
          <Input label="confirmar contraseña" />
          <Button title="Regístrate" margin />
          <View style={{flexDirection: 'row'}}>
            <Text style={[styles.bottomText, {color: '#7A7A7A'}]}>
              ya tienes cuenta?{' '}
            </Text>
            <TouchableOpacity onPress={() => navigation.popToTop()}>
              <Text style={[styles.bottomText, {color: colors.tint2}]}>
                iniciar
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default RegisterScreen;
