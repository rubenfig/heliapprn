import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Platform,
  Alert,
} from 'react-native';
import {colors, perfectSize} from '../../utils/styling';
import {Images} from '../../assets/images';
import {RoundedInput} from '../../components/RoundedInput';
import {Button} from '../../components/Button';
import {HR} from '../../components/HR';
import OneSignal from 'react-native-onesignal';
import * as LocalAuthentication from 'expo-local-authentication';
import {UserService} from '../../api/userService';
import {UserContext} from '../../utils/UserContext';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
  },
  icon: {
    width: perfectSize(50),
    height: perfectSize(50),
  },
  iconText: {
    marginTop: 4,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(14),
    lineHeight: perfectSize(20),
    textAlign: 'center',
    color: '#000',
  },
  bottomText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    color: colors.tint2,
    marginBottom: perfectSize(90),
  },
});

const LoginScreen = ({navigation}) => {
  const {setUser} = useContext(UserContext);
  const [account, setAccount] = useState({
    pass: '',
    user: '',
    uid: '',
    terminal: 'android',
  });
  const [recordar] = useState(false);
  const [loading, setLoading] = useState(false);
  const [biometrics, setBiometrics] = useState({
    hasFingerprint: false,
    hasFace: false,
  });
  useState(() => {
    OneSignal.init(
      Platform.OS === 'android'
        ? '374280ff-1dc2-4dd9-9a3a-530627c62d2d'
        : 'a10607e5-4f14-4129-9922-61dd289b5442',
      {kOSSettingsKeyAutoPrompt: true},
    ); // set kOSSettingsKeyAutoPrompt to false prompting manually on iOS
    OneSignal.addEventListener('ids', ids => {
      console.log(ids);
      setAccount({...account, uuid: ids.userId, terminal: Platform.OS});
      OneSignal.removeEventListener('ids');
    });
    UserService.getUser().then(user => {
      if (user) {
        LocalAuthentication.supportedAuthenticationTypesAsync().then(res => {
          setBiometrics({
            hasFingerprint: res.includes(1),
            hasFace: res.includes(2),
          });
        });
      }
    });
  }, []);

  const login = async () => {
    setLoading(true);
    try {
      const resp = await UserService.login(account);
      console.log(resp);
      if (resp.status === 0) {
        await UserService.setRecordado(recordar);
        if (resp.usuario.primerPassword) {
          navigation.navigate(['CambiarPassword', {passActual: account.pass}]);
        } else {
          // if (this.hasKeychain && !this.hasKeychainPassword && this.touchHabilitado) {
          //   this.touch.save('HELIAPP', this.account.pass);
          //   localStorage.setItem('touchHabilitado', 'true');
          // }
          const taller =
            resp.perfil && resp.perfil.toLowerCase().includes('taller');
          setUser(resp);
          navigation.navigate('Home');
        }
      } else {
        Alert.alert(resp.mensaje);
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const loginBiometrics = async () => {
    const authenticated = await LocalAuthentication.authenticateAsync({
      promptMessage: 'Por favor complete la autenticación',
      cancelLabel: 'Cancelar',
      fallbackLabel: '',
      disableDeviceFallback: false,
    });
    console.log(authenticated);
    if (authenticated) {
      const user = await UserService.getUser();
      setAccount({...account, user: user?.usuario?.usuario, pass: user?.pass});
      await login();
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1, justifyContent: 'flex-end'}}>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Text style={styles.header}>Iniciar Sesión</Text>
          <RoundedInput
            value={account.user}
            onChange={value => setAccount({...account, user: value})}
            icon={Images.avatar}
            placeholder={'nombre'}
            autoCapitalize="none"
          />
          <RoundedInput
            value={account.pass}
            onChange={value => setAccount({...account, pass: value})}
            icon={Images.lock}
            placeholder={'contraseña'}
            autoCompleteType="password"
            secureTextEntry={true}
          />
          <Button
            title="Iniciar Sesión"
            loading={loading}
            margin
            onPress={login}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <HR />
          <HR />
        </View>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Button
            title="Crear cuenta"
            margin
            onPress={() => navigation.navigate('Register')}
          />
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginVertical: perfectSize(25),
        }}>
        {biometrics.hasFace && (
          <TouchableOpacity
            style={{marginHorizontal: 16}}
            onPress={loginBiometrics}>
            <Image source={Images.face} style={styles.icon} />
            <Text style={styles.iconText}>Face ID</Text>
          </TouchableOpacity>
        )}
        {biometrics.hasFingerprint && (
          <TouchableOpacity
            style={{marginHorizontal: 16}}
            onPress={loginBiometrics}>
            <Image source={Images.fingerprint} style={styles.icon} />
            <Text style={styles.iconText}>Touch ID</Text>
          </TouchableOpacity>
        )}
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('RecuperarPassword')}>
        <Text style={styles.bottomText}>Olvidaste tu contraseña?</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default LoginScreen;
