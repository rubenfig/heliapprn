import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import Select from '../../../components/Select';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {AeronavesService} from '../../../api/aeronavesService';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});
const dias = ['0', '1', '2', '3', '4', '5', '6', '7'];
const horas = '00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'.split(
  ',',
);
const CrearMantenimientoScreen = ({navigation, route}) => {
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [aeronaveElegida, setAeronaveElegida] = useState(null);
  const [aeronaves, setAeronaves] = useState([]);
  const [mantenimiento, setMantenimiento] = useState({
    inicio: new Date(),
    fase: '3',
  });

  React.useEffect(() => {
    return navigation.addListener('focus', () => {
      console.log('test');
      listarAviones();
    });
  }, [navigation]);

  const listarAviones = async () => {
    setLoading(true);
    try {
      const listarFunc = await AeronavesService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setAeronaves(response);
        if (response.length > 0) {
          setAeronaveElegida(response[0]);
        }
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const agendar = async () => {
    try {
      setLoading(true);
      const newMantenimiento = {...mantenimiento};
      const horasM = parseInt(newMantenimiento.horas);
      const diasM = parseInt(newMantenimiento.dias);
      newMantenimiento.fin = moment(newMantenimiento.inicio)
        .add(diasM, 'd')
        .add(horasM, 'hours');
      newMantenimiento.inicio = moment(newMantenimiento.inicio).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      newMantenimiento.fin = newMantenimiento.fin.format('YYYY-MM-DD HH:mm:ss');
      newMantenimiento.idAeronave = aeronaveElegida.idAeronave;
      delete newMantenimiento.horas;
      delete newMantenimiento.dias;
      console.log(newMantenimiento);
      const res = await AeronavesService.agendarMantenimiento(newMantenimiento);
      if (res?.data?.estado === 0) {
        Alert.alert('Mantenimiento agendado exitosamente!');
        navigation.goBack();
        route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Select
            onValueChange={itemValue => {
              setAeronaveElegida(itemValue);
            }}
            label={'aeronave'}
            value={aeronaveElegida}
            placeholder={'Elija una aeronave'}
            items={aeronaves?.map(a => {
              return {key: a.idAeronave, label: a.descripcion, value: a};
            })}
          />
          <Input
            label="fecha"
            value={moment(mantenimiento.inicio).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowDatePicker(true)}
          />
          <Input
            label="inicio"
            value={moment(mantenimiento.inicio).format('HH:mm')}
            disabled
            onPress={() => setShowTimePicker(true)}
          />
          <Select
            onValueChange={itemValue => {
              setMantenimiento({...mantenimiento, dias: itemValue});
            }}
            label={'días'}
            value={mantenimiento.dias}
            placeholder={'Elija la cantidad de días'}
            items={dias.map(d => {
              return {key: d, label: d, value: d};
            })}
          />
          <Select
            onValueChange={itemValue => {
              setMantenimiento({...mantenimiento, horas: itemValue});
            }}
            label={'horas'}
            value={mantenimiento.horas}
            placeholder={'Elija la cantidad de horas'}
            items={horas.map(d => {
              return {key: d, label: d, value: d};
            })}
          />
          <Input
            label={'motivo'}
            value={mantenimiento.motivo}
            onChange={value =>
              setMantenimiento({...mantenimiento, motivo: value})
            }
          />
          {showDatePicker && (
            <DateTimePicker
              value={mantenimiento.inicio}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowDatePicker(false);
                date && setMantenimiento({...mantenimiento, inicio: date});
              }}
            />
          )}
          {showTimePicker && (
            <DateTimePicker
              value={mantenimiento.inicio}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                setShowTimePicker(false);
                date && setMantenimiento({...mantenimiento, inicio: date});
              }}
            />
          )}
          <Button
            title="agendar mantenimiento"
            margin
            onPress={() => agendar()}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CrearMantenimientoScreen;
