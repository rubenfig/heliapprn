import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, View, StyleSheet, Alert, Text} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../components/ItemList';
import {numberFormat} from '../../utils/utils';
import moment from 'moment';
import {AeronavesService} from '../../api/aeronavesService';

const MantenimientosListadoScreen = ({navigation}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [mantenimientos, setMantenimientos] = useState([]);

  useEffect(() => {
    listarMantenimientos();
    navigation.setParams({refresh: listarMantenimientos});
  }, []);

  const listarMantenimientos = async () => {
    setLoading(true);
    try {
      const listarFunc = await AeronavesService.listarMantenimientos();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevosMantenimientos = response.lista.map(a => {
          return {
            ...a,
            id: a.idMantenimiento,
            title: a.aeronave?.descripcion,
            subtitle: 'Motivo: ' + a.motivo,
          };
        });
        setMantenimientos(nuevosMantenimientos);
        itemList?.current?.reset();
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const renderMoreInfo = item => {
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>Correo: {item.correo}</Text>
        <Text style={itemListStyles.subtitleStyle}>
          Horas de vuelo:{' '}
          {numberFormat.format(item?.aeronave?.horasVueloDisponible)}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Fecha del mantenimiento:{' '}
          {moment(item.fechaMantenimiento).format('DD-MM-YYYY HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Final del mantenimiento: {moment(item.fin).format('DD-MM-YYYY HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Días de mantenimiento: {item.diasMantenimiento}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>Fase: {item.fase}</Text>
      </View>
    );
  };
  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <ItemList
        ref={itemList}
        items={mantenimientos}
        renderMoreInfo={renderMoreInfo}
      />
    </View>
  );
};

export default MantenimientosListadoScreen;
