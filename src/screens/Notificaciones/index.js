import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Text,
} from 'react-native';
import {styles as itemListStyles} from '../../components/ItemList';
import {colors, elevationShadowStyle} from '../../utils/styling';
import {NotificacionesService} from '../../api/notificacionesService';
import {UserContext} from '../../utils/UserContext';
import {ReservaService} from '../../api/reservaService';
import {reservaEstados} from '../../utils/utils';
import moment from 'moment';

const NotificacionesListadoScreen = () => {
  const [loading, setLoading] = useState(false);
  const {user} = useContext(UserContext);
  const [notificaciones, setNotificaciones] = useState([]);

  useEffect(() => {
    listarNotificaciones();
  }, []);

  const listarNotificaciones = async () => {
    setLoading(true);
    try {
      const listarFunc = await NotificacionesService.listarNotificaciones(
        user?.usuario?.idUsuario,
      );
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        setNotificaciones(response?.lista ? response.lista : []);
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const verReserva = async reserva => {
    setLoading(true);
    try {
      const listarFunc = await ReservaService.obtenerReserva(reserva);
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        if (response.estado === 0) {
          if (response.lista.length === 0) {
            Alert.alert('No se encontró la reserva');
          } else {
            const reservaObtenida = response.lista[0];
            for (const estado of reservaEstados) {
              if (estado.idEstadoReserva === reservaObtenida.estadoReserva) {
                reservaObtenida.estado = estado;
              }
            }
            // this.router.navigate(['/reservas-detalle', { reserva: JSON.stringify(reservaObtenida) }]);
          }
        } else {
          Alert.alert(response.mensaje || 'Ocurrió un error indeterminado');
        }
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };
  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView
        contentContainerStyle={{
          padding: 8,
        }}>
        {notificaciones &&
          notificaciones.map((noti, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => verReserva(noti.idReserva)}
              style={{
                padding: 16,
                marginVertical: 4,
                backgroundColor: '#fff',
                flexDirection: 'row',
                alignItems: 'center',
                ...elevationShadowStyle(2),
              }}>
              <View style={{flex: 1, marginHorizontal: 16}}>
                <Text style={itemListStyles.titleStyle}>
                  Reserva {noti.idReserva}
                </Text>
                <Text style={itemListStyles.subtitleStyle}>
                  {noti?.fechaNotificacion &&
                    moment(noti.fechaNotificacion).format('DD-MM-YYYY')}
                </Text>
                <Text style={itemListStyles.subtitleStyle}>{noti.mensaje}</Text>
              </View>
            </TouchableOpacity>
          ))}
      </ScrollView>
    </View>
  );
};

export default NotificacionesListadoScreen;
