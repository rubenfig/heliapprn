import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  CheckBox,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {AeronavesService} from '../../../api/aeronavesService';
import {Overlay} from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const AgendarCierrePistaScreen = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [showDateFinPicker, setShowDateFinPicker] = useState(false);
  const [showTimeFinPicker, setShowTimeFinPicker] = useState(false);
  const [aeronaves, setAeronaves] = useState([]);
  const [mostrarAeronaves, setMostrarAeronaves] = useState(false);
  const [cierrePista, setCierrePista] = useState({
    id: route.params?.pista?.id,
    inicio: new Date(),
    fin: new Date(),
    aeronaves: [],
  });

  React.useEffect(() => {
    return navigation.addListener('focus', () => {
      console.log('test');
      listarAviones();
    });
  }, [navigation]);

  const listarAviones = async () => {
    setLoading(true);
    try {
      const listarFunc = await AeronavesService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setAeronaves(response);
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const agendar = async () => {
    try {
      setLoading(true);
      const newCierrePista = {...cierrePista};
      if (moment(newCierrePista.inicio).isAfter(moment(newCierrePista.fin))) {
        Alert.alert('La fecha inicio no puede ser después de fecha fin');
        return;
      }
      newCierrePista.inicio = moment(newCierrePista.inicio).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      newCierrePista.fin = moment(newCierrePista.fin).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      newCierrePista.aeronaves = aeronaves
        .filter(a => a.seleccionado)
        .map(avion => avion.idAeronave);
      console.log(newCierrePista);
      const res = await AeronavesService.agendarCierrePista(newCierrePista);
      if (res?.data?.estado === 0) {
        Alert.alert('CierrePista agendado exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const AeronaveCheckbox = ({aeronave}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={[styles.infoText, {flex: 1}]}>{aeronave.descripcion}</Text>
        <CheckBox
          value={aeronave.seleccionado}
          onChange={() => {
            const newAeronaves = aeronaves.map(a => {
              if (a.idAeronave === aeronave.idAeronave) {
                return {...a, seleccionado: !a.seleccionado};
              } else return a;
            });
            setAeronaves(newAeronaves);
          }}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <Overlay
        isVisible={mostrarAeronaves}
        overlayStyle={{
          paddingVertical: 32,
          paddingHorizontal: 16,
          height: 'auto',
        }}>
        <ScrollView>
          {aeronaves.map(aeronave => (
            <AeronaveCheckbox aeronave={aeronave} />
          ))}
          <Button
            title={'Cerrar'}
            containerStyle={{marginTop: 32}}
            onPress={() => setMostrarAeronaves(false)}
          />
        </ScrollView>
      </Overlay>
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="aeronaves"
            value={aeronaves
              .filter(a => a.seleccionado)
              .map(a => a.descripcion)
              .join(', ')}
            disabled
            onPress={() => setMostrarAeronaves(true)}
          />
          <Input
            label="fecha de inicio"
            value={moment(cierrePista.inicio).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowDatePicker(true)}
          />
          <Input
            label="hora de inicio"
            value={moment(cierrePista.inicio).format('HH:mm')}
            disabled
            onPress={() => setShowTimePicker(true)}
          />
          <Input
            label="fecha de fin"
            value={moment(cierrePista.fin).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowDateFinPicker(true)}
          />
          <Input
            label="hora de fin"
            value={moment(cierrePista.fin).format('HH:mm')}
            disabled
            onPress={() => setShowTimeFinPicker(true)}
          />
          {showDatePicker && (
            <DateTimePicker
              value={cierrePista.inicio}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowDatePicker(false);
                date && setCierrePista({...cierrePista, inicio: date});
              }}
            />
          )}
          {showTimePicker && (
            <DateTimePicker
              value={cierrePista.inicio}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                setShowTimePicker(false);
                date && setCierrePista({...cierrePista, inicio: date});
              }}
            />
          )}
          {showDateFinPicker && (
            <DateTimePicker
              value={cierrePista.fin}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowDateFinPicker(false);
                date && setCierrePista({...cierrePista, fin: date});
              }}
            />
          )}
          {showTimeFinPicker && (
            <DateTimePicker
              value={cierrePista.fin}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                setShowTimeFinPicker(false);
                date && setCierrePista({...cierrePista, fin: date});
              }}
            />
          )}
          <Button
            title="Agendar cierre"
            margin
            onPress={() =>
              Alert.alert('Está seguro?', null, [
                {text: 'Cancelar'},
                {text: 'Ok', onPress: () => agendar()},
              ])
            }
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AgendarCierrePistaScreen;
