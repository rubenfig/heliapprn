import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, View, StyleSheet, Alert} from 'react-native';
import {ItemList} from '../../components/ItemList';
import {AeronavesService} from '../../api/aeronavesService';

const PistasListadoScreen = ({navigation}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [pistas, setPistas] = useState([]);

  useEffect(() => {
    listarPistas();
    navigation.setParams({refresh: listarPistas});
  }, []);

  const listarPistas = async () => {
    setLoading(true);
    try {
      const listarFunc = await AeronavesService.listarPistas();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevosPistas = response.lista.map(a => {
          return {
            ...a,
            id: a.id,
            title: a.descripcion,
            subtitle: a.estado ? 'Activo' : 'Inactivo',
          };
        });
        setPistas(nuevosPistas);
        itemList?.current?.reset();
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <ItemList
        ref={itemList}
        items={pistas}
        leftButton={'Agendar cierre'}
        onLeftPress={item =>
          navigation.navigate('AgendarCierrePista', {pista: item})
        }
      />
    </View>
  );
};

export default PistasListadoScreen;
