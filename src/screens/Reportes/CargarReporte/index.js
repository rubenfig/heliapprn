import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Linking,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import Select from '../../../components/Select';
import {ReservaService} from '../../../api/reservaService';
import {ReportesService} from '../../../api/reportesService';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const CargarReporteScreen = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [idReporteTipo] = useState(route.params?.idReporteTipo);
  const [reporte] = useState(route.params?.reporte);
  const [parametros, setParametros] = useState(route.params?.parametros);
  const [idReserva, setIdReserva] = useState(reporte?.idReserva);
  const [reserva, setReserva] = useState(null);

  useEffect(() => {
    if (reporte) {
      buscarReserva();
    }
  }, []);

  const buscarReserva = async () => {
    setLoading(true);
    try {
      const listarFunc = await ReservaService.obtenerReserva(idReserva);
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data;
        if (response.estado === 0) {
          if (response.lista?.length === 0) {
            setReserva(null);
            Alert.alert(response.mensaje || 'Ocurrió un error indeterminado');
          } else {
            setReserva(response.lista[0]);
          }
        } else {
          setReserva(null);
          Alert.alert(response.mensaje || 'Ocurrió un error indeterminado');
        }
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const cargarReporte = async () => {
    try {
      setLoading(true);
      if (!reserva) {
        Alert.alert('Debe elegir una reserva existente');
        return;
      }
      const newReporte = {
        idReserva: reserva?.idReserva,
        idReporteTipo: parseInt(idReporteTipo),
        valores: parametros,
      };
      console.log(newReporte);
      let res = await ReportesService.cargarReporte(newReporte);
      if (res?.data?.estado === 0) {
        Alert.alert('Reporte cargado exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const descargarReporte = async () => {
    const url = ReportesService.descargarReporte(reporte?.idReporte);
    await Linking.openURL(url);
  };

  const renderCargarReporte = () => {
    return (
      <View>
        {parametros &&
          parametros.map((parametro, index) =>
            parametro.tipoDato === 'string' ? (
              <Input
                label={parametro.parametro}
                value={parametro.valor}
                onChange={text => {
                  const newParametros = [...parametros];
                  parametros[index].valor = text;
                  setParametros(newParametros);
                }}
              />
            ) : (
              <Select
                onValueChange={itemValue => {
                  const newParametros = [...parametros];
                  parametros[index].valor = itemValue;
                  setParametros(newParametros);
                }}
                label={parametro.parametro}
                value={parametro.valor}
                placeholder={'Elija un valor'}
                items={[
                  {key: 1, label: 'Satisface', value: 'true'},
                  {key: 2, label: 'No satisface', value: 'false'},
                ]}
              />
            ),
          )}
        <Button title="cargar reporte" margin onPress={() => cargarReporte()} />
      </View>
    );
  };

  const renderVerReporte = () => {
    return (
      <View>
        {reporte?.categorias?.map(dato => (
          <Input
            editable={false}
            selectTextOnFocus={false}
            label={dato.parametro}
            value={
              dato.valor
                ? dato.tipoDato === 'boolean'
                  ? dato.valor === 'true'
                    ? 'Satisface'
                    : 'No satisface'
                  : dato.valor
                : ''
            }
          />
        ))}
        <Button
          title="descargar reporte"
          margin
          onPress={() => descargarReporte()}
        />
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="id de la reserva"
            value={idReserva + ''}
            onChange={text => setIdReserva(text)}
            editable={!reporte}
            selectTextOnFocus={!reporte}
            onBlur={buscarReserva}
          />
          {reserva?.alumno && (
            <Input
              label="alumno"
              editable={!reporte}
              selectTextOnFocus={!reporte}
              value={reserva.alumno?.nombreCompleto}
            />
          )}
          {reserva?.fechaReserva && (
            <Input
              label="fecha"
              editable={!reporte}
              selectTextOnFocus={!reporte}
              value={moment(reserva.fechaReserva).format('DD-MM-YYYY')}
            />
          )}
          {reserva?.aeronave && (
            <Input
              label="aeronave"
              editable={!reporte}
              selectTextOnFocus={!reporte}
              value={reserva.aeronave?.descripcion}
            />
          )}
          {reporte ? renderVerReporte() : renderCargarReporte()}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CargarReporteScreen;
