import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Text,
} from 'react-native';
import {styles as itemListStyles} from '../../../components/ItemList';
import {ReportesService} from '../../../api/reportesService';
import {colors, elevationShadowStyle} from '../../../utils/styling';

const TiposReportesListadoScreen = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [tiposReportes, setTiposReportes] = useState([]);

  useEffect(() => {
    listarTiposReportes();
  }, []);

  const listarTiposReportes = async () => {
    setLoading(true);
    try {
      const listarFunc = await ReportesService.listarFormularios();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        setTiposReportes(response?.lista ? response.lista : []);
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const listarParametros = async formulario => {
    setLoading(true);
    try {
      const listarFunc = await ReportesService.listarParametros(
        formulario.idReporteTipo,
      );
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        if (response.estado === 0) {
          if (response.lista.length === 0) {
            Alert.alert('No se encontró el formulario');
          } else {
            navigation.navigate('CargarReporte', {
              parametros: response.lista,
              idReporteTipo: formulario.idReporteTipo,
              refresh: () => {
                navigation.goBack();
                route?.params?.refresh && route?.params?.refresh();
              },
            });
          }
        } else {
          Alert.alert(response.mensaje || 'Ocurrió un error indeterminado!');
        }
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView
        contentContainerStyle={{
          padding: 8,
        }}>
        {tiposReportes &&
          tiposReportes.map((tipo, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => listarParametros(tipo)}
              style={{
                padding: 16,
                marginVertical: 4,
                backgroundColor: '#fff',
                flexDirection: 'row',
                alignItems: 'center',
                ...elevationShadowStyle(2),
              }}>
              <View style={{flex: 1, marginHorizontal: 16}}>
                <Text style={itemListStyles.titleStyle}>{tipo.nombre}</Text>
                <Text style={itemListStyles.subtitleStyle}>
                  {tipo.descripcion}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
      </ScrollView>
    </View>
  );
};

export default TiposReportesListadoScreen;
