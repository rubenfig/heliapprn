import React, {useRef, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  Text,
  StatusBar,
} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../components/ItemList';
import {SearchBar} from 'react-native-elements';
import {colors} from '../../utils/styling';
import {ReportesService} from '../../api/reportesService';
import moment from 'moment';

const ReportesListadoScreen = ({navigation, route}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [filtro, setFiltro] = useState(null);
  const [reportes, setReportes] = useState([]);

  const renderMoreInfo = item => {
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>
          Fecha: {moment(item.fecha).format('DD-MM-YYYY')}
        </Text>
      </View>
    );
  };

  const listarReportes = async () => {
    setLoading(true);
    try {
      let listarFunc;
      if (route.params?.idAlumno) {
        listarFunc = await ReportesService.listarReportesAlumno(
          route.params?.idAlumno,
        );
      } else if (filtro) {
        listarFunc = await ReportesService.listarReportesReserva(filtro);
      }
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        if (response?.length === 0) {
          Alert.alert('No se encontraron reportes o la reserva no existe');
          return;
        }
        const nuevosReportes = response.map(a => {
          return {
            ...a,
            id: a.idReporte,
            title: 'Reserva: ' + a.idReserva,
            subtitle: 'Tipo: ' + a.tipo.nombre,
          };
        });
        setReportes(nuevosReportes);
        itemList?.current?.reset();
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const goToReporte = reporte => {
    const newReporte = {
      ...reporte,
      categorias: reporte.categorias.reduce(
        (result, c) => [
          ...result,
          ...c.valores.map(v => {
            return {...v.parametro, valor: v.valor};
          }),
        ],
        [],
      ),
    };
    console.log(newReporte);
    navigation.navigate('CargarReporte', {
      reporte: newReporte,
    });
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      {!route.params?.idAlumno && (
        <SearchBar
          inputContainerStyle={{backgroundColor: colors.white}}
          containerStyle={{
            backgroundColor: colors.primary,
            borderBottomWidth: 0,
            borderTopWidth: 0,
          }}
          placeholder="Buscar por ID reserva..."
          onChangeText={setFiltro}
          onSubmitEditing={listarReportes}
          value={filtro}
        />
      )}
      <ItemList
        ref={itemList}
        items={reportes}
        rightButton={'ver detalle'}
        onRightPress={item => goToReporte(item)}
        renderMoreInfo={renderMoreInfo}
      />
    </View>
  );
};

export default ReportesListadoScreen;
