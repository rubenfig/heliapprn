import React, {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  Text,
  StatusBar,
} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../../components/ItemList';
import {reservaEstados} from '../../../utils/utils';
import {ReservaService} from '../../../api/reservaService';
import moment from 'moment';
import {MisionesService} from '../../../api/misionesService';
import {InstructoresService} from '../../../api/instructoresService';
import RNPickerSelect from 'react-native-picker-select';
import {Overlay} from 'react-native-elements';
import {Input} from '../../../components/Input';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    ...itemListStyles.subtitleStyle,
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    ...itemListStyles.subtitleStyle,
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const AprobarReservasScreen = ({}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [reservaRechazo, setReservaRechazo] = useState(null);
  const [tipoRechazoElegido, setTipoRechazoElegido] = useState(null);
  const [motivoRechazo, setMotivoRechazo] = useState(null);
  const [reservas, setReservas] = useState([]);
  const [misiones, setMisiones] = useState([]);
  const [instructores, setInstructores] = useState([]);
  const [tiposRechazo, setTiposRechazo] = useState([]);

  useEffect(() => {
    listarReservas();
    listarInstructores();
    listarMisiones();
    listarTiposRechazo();
  }, []);
  const listarMisiones = async () => {
    try {
      const response = await MisionesService.listar();
      setMisiones(response?.data?.lista);
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado!');
    }
  };

  const listarTiposRechazo = async () => {
    try {
      const response = await ReservaService.listarRechazo();
      setTiposRechazo(response?.data?.lista);
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado!');
    }
  };

  const listarInstructores = async () => {
    try {
      const response = await InstructoresService.listar();
      setInstructores(response?.data?.lista);
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado!');
    }
  };

  const listarReservas = async () => {
    setLoading(true);
    try {
      const listarFunc = await ReservaService.listarReservasPendientes();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        console.log(response);
        const nuevosReservas = response.lista.map(a => {
          for (const estado of reservaEstados) {
            if (estado.idEstadoReserva === a.estadoReserva) {
              a.estado = estado;
            }
          }
          return {
            ...a,
            id: a.idReserva,
            title: a.idReserva,
            subtitle: a.aeronave?.descripcion,
          };
        });
        setReservas(nuevosReservas);
        itemList?.current?.reset();
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const limpiarRechazo = () => {
    setTipoRechazoElegido(null);
    setReservaRechazo(null);
    setMotivoRechazo(null);
  };

  const rechazarReserva = async () => {
    try {
      const data = {
        idReserva: reservaRechazo.idReserva,
        idInstructor: reservaRechazo.instructor
          ? reservaRechazo.instructor.idInstructor
          : reservaRechazo.idInstructor,
        estadoReserva: 3,
        idMision: reservaRechazo.mision
          ? reservaRechazo.mision.idMision
          : reservaRechazo.idMision,
        motivoRechazo: motivoRechazo,
        tipoRechazo: tipoRechazoElegido.idTipoRechazo,
      };
      setLoading(true);
      const response = await ReservaService.modificarEstado(data);
      if (response.data && response.data.estado === 0) {
        setMotivoRechazo(null);
        setReservaRechazo(null);
        setTipoRechazoElegido(null);
        Alert.alert('Reserva modificada exitosamente!');
        await listarReservas();
      } else {
        Alert.alert(
          response?.data?.mensaje || 'Ocurrió un error indeterminado',
        );
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const modificarReserva = async (reserva, estado, motivo?, tipoRechazo?) => {
    try {
      const data = {
        idReserva: reserva.idReserva,
        idInstructor: reserva.instructor
          ? reserva.instructor.idInstructor
          : reserva.idInstructor,
        estadoReserva: estado,
        idMision: reserva.mision ? reserva.mision.idMision : reserva.idMision,
        motivoRechazo: motivo,
        tipoRechazo: tipoRechazo,
      };
      console.log(data, reserva);
      setLoading(true);
      const response = await ReservaService.modificarEstado(data);
      if (response.data && response.data.estado === 0) {
        Alert.alert('Reserva modificada exitosamente!');
        await listarReservas();
      } else {
        Alert.alert(
          response?.data?.mensaje || 'Ocurrió un error indeterminado',
        );
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const renderMoreInfo = reserva => {
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>
          Desde: {reserva.inicio && moment(reserva.inicio).format('HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Hasta: {reserva.fin && moment(reserva.fin).format('HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Fecha:{' '}
          {reserva.fechaReserva &&
            moment(reserva.fechaReserva).format('DD-MM-YYYY')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Alumno: {reserva.alumno?.nombreCompleto}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Estado: {reserva.estado?.descripcion}
        </Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={itemListStyles.subtitleStyle}>Instructor: </Text>
          <View style={{flex: 1}}>
            <RNPickerSelect
              onValueChange={itemValue => {
                const newReservas = reservas.map(r => {
                  return r.id === reserva.id
                    ? {...r, instructor: itemValue}
                    : r;
                });
                setReservas(newReservas);
              }}
              value={reserva.instructor}
              placeholder={{
                label: 'Elija un instructor',
                key: null,
                value: null,
              }}
              style={pickerSelectStyles}
              useNativeAndroidPickerStyle={false}
              items={instructores?.map(i => {
                return {key: i.idInstructor, label: i.nombreCompleto, value: i};
              })}
            />
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={itemListStyles.subtitleStyle}>Misión: </Text>
          <View style={{flex: 1}}>
            <RNPickerSelect
              onValueChange={itemValue => {
                console.log(itemValue);
                const newReservas = reservas.map(r => {
                  return r.id === reserva.id ? {...r, mision: itemValue} : r;
                });
                console.log(newReservas);
                setReservas(newReservas);
              }}
              value={reserva.mision}
              placeholder={{label: 'Elija una misión', key: null, value: null}}
              style={pickerSelectStyles}
              useNativeAndroidPickerStyle={false}
              items={misiones?.map(m => {
                return {key: m.idMision, label: m.descripcion, value: m};
              })}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <Overlay
        isVisible={!!reservaRechazo}
        overlayStyle={{
          paddingVertical: 32,
          paddingHorizontal: 16,
          height: 'auto',
        }}>
        {loading && (
          <ActivityIndicator
            style={[StyleSheet.absoluteFill, {zIndex: 10}]}
            size={'large'}
          />
        )}
        <Text style={[itemListStyles.titleStyle, {textAlign: 'center'}]}>
          Rechazar reserva
        </Text>
        <View
          style={{
            marginTop: 40,
          }}>
          <Text
            style={{
              color: '#666666',
              fontFamily: 'Questrial-Regular',
              fontSize: perfectSize(18),
              lineHeight: perfectSize(23),
            }}>
            Tipo de rechazo
          </Text>
          <RNPickerSelect
            onValueChange={itemValue => {
              setTipoRechazoElegido(itemValue);
            }}
            value={tipoRechazoElegido}
            placeholder={{
              label: 'Elija un tipo de rechazo',
              key: null,
              value: null,
            }}
            style={{
              inputIOS: {
                paddingVertical: 0,
                paddingRight: 30,
                borderBottomWidth: 1,
                borderBottomColor: '#E6E6E6',
                fontFamily: 'Questrial-Regular',
                fontSize: perfectSize(18),
                lineHeight: perfectSize(23),
              },
              inputAndroid: {
                paddingVertical: 0,
                paddingRight: 30,
                borderBottomWidth: 1,
                borderBottomColor: '#E6E6E6',
                fontFamily: 'Questrial-Regular',
                fontSize: perfectSize(18),
                lineHeight: perfectSize(23),
              },
            }}
            useNativeAndroidPickerStyle={false}
            items={tiposRechazo?.map(m => {
              return {key: m.idTipoRechazo, label: m.descripcion, value: m};
            })}
          />
        </View>
        <Input
          value={motivoRechazo}
          label="Motivo"
          onChange={setMotivoRechazo}
        />
        <Text style={[itemListStyles.subtitleStyle, {textAlign: 'center'}]}>
          Obs: se aplicaran recargos al cancelar una reserva
        </Text>
        <Button
          title={'Rechazar reserva'}
          containerStyle={{marginTop: 32}}
          onPress={() => rechazarReserva()}
        />
        <Button
          title={'Cancelar'}
          containerStyle={{marginTop: 32}}
          onPress={() => limpiarRechazo()}
        />
      </Overlay>
      <ItemList
        ref={itemList}
        items={reservas}
        leftButton={'Aprobar'}
        rightButton={'rechazar'}
        onLeftPress={item => modificarReserva(item, 2)}
        onRightPress={item => setReservaRechazo(item)}
        renderMoreInfo={renderMoreInfo}
      />
    </View>
  );
};

export default AprobarReservasScreen;
