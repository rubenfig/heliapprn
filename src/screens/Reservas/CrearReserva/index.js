import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Alert,
  ActivityIndicator,
  Text,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import {UserContext} from '../../../utils/UserContext';
import {AeronavesService} from '../../../api/aeronavesService';
import {AlumnosService} from '../../../api/alumnosService';
import Select from '../../../components/Select';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {Overlay} from 'react-native-elements';
import {ReservaService} from '../../../api/reservaService';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const CrearReservaScreen = ({navigation, route}) => {
  console.log(route.params);
  const {user} = useContext(UserContext);
  const [mostrarAlert, setMostrarAlert] = useState(false);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showTimePicker, setShowTimePicker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [alumno, setAlumno] = useState('');
  const [aeronaves, setAeronaves] = useState([]);
  const [aeronaveElegida, setAeronaveElegida] = useState(null);
  const [alumnos, setAlumnos] = useState([]);
  const [alumnoElegido, setAlumnoElegido] = useState(null);
  const [reserva, setReserva] = useState({
    horaInicio: route.params?.selectedDate?.toDate() || new Date(),
    fechaReserva: route.params?.selectedDate?.toDate() || new Date(),
    bloque: '1',
  });
  const perfil = user.perfil.toLowerCase();
  useEffect(() => {
    if (perfil.includes('secretaria')) {
      listarAlumnos();
    } else {
      setAlumno(user.usuario.nombreCompleto);
      if (user?.alumno?.idAlumno) {
        setReserva({...reserva, idAlumno: user.alumno.idAlumno});
      }
    }
    // this.route.paramMap.subscribe(
    //   (params) => {
    //     const fecha = params.get('fecha');
    //     this.reserva.fechaReserva = fecha;
    //     this.reserva.horaInicio = fecha;
    //   }
    // );
    listarAviones();
  }, []);

  const listarAviones = async () => {
    setLoading(true);
    try {
      const listarFunc = await AeronavesService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setAeronaves(response);
        if (response.length > 0) {
          setAeronaveElegida(response[0]);
        }
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const listarAlumnos = async () => {
    setLoading(true);
    try {
      const listarFunc = await AlumnosService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setAlumnos(response);
        if (response.length > 0) {
          setAlumnoElegido(response[0]);
        }
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const mostrarAlertReserva = () => {
    try {
      const fecha = moment(reserva.fechaReserva).format('DD-MM-YYYY');
      const fechaDesde = moment(reserva.horaInicio);
      // tslint:disable-next-line:radix
      const momentHasta = moment(fechaDesde).add(
        parseInt(reserva.bloque) * 90,
        'minutes',
      );
      // tslint:disable-next-line:radix
      const costo = aeronaveElegida.costoPorPeriodo * parseInt(reserva.bloque);
      const desde = fechaDesde.format('HH:mm');
      const hasta = momentHasta.format('HH:mm');
      return (
        <View>
          <Text style={styles.header}>Crear la reserva?</Text>
          <Text style={styles.infoText}>Día: {fecha}</Text>
          <Text style={styles.infoText}>Desde: {desde}</Text>
          <Text style={styles.infoText}>Hasta: {hasta}</Text>
          <Text style={styles.infoText}>
            Aeronave: {aeronaveElegida.descripcion}
          </Text>
          <Text style={styles.infoText}>Costo: {costo}</Text>
          <Button
            title={'confirmar'}
            containerStyle={{marginTop: 32}}
            onPress={() => crearReserva()}
          />
          <Button
            title={'Cancelar'}
            containerStyle={{marginTop: 32}}
            onPress={() => setMostrarAlert(false)}
          />
        </View>
      );
    } catch (e) {
      setMostrarAlert(false);
    }
  };

  const crearReserva = async () => {
    try {
      setLoading(true);
      const newReserva = {...reserva};
      newReserva.fechaReserva = moment(newReserva.fechaReserva).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      newReserva.horaInicio =
        moment(newReserva.fechaReserva).format('YYYY-MM-DD ') +
        moment(newReserva.horaInicio).format('HH:mm:ss');
      newReserva.idAeronave = aeronaveElegida.idAeronave;
      if (!newReserva.idAlumno) {
        Alert.alert('Se debe seleccionar un alumno para la reserva');
        setLoading(false);
        return;
      }
      if (alumnoElegido.tipo === 'temporal') {
        if (
          !newReserva.nombre ||
          newReserva.nombre.length === 0 ||
          !newReserva.apellido ||
          newReserva.apellido.length === 0 ||
          !newReserva.documento ||
          newReserva.documento.length === 0
        ) {
          Alert.alert('Debe completar los datos del alumno para la reserva');
          setLoading(false);
          return;
        }
      }
      console.log(JSON.stringify(newReserva));
      const res = await ReservaService.crearReserva(newReserva);
      if (res?.data?.estado === 0) {
        Alert.alert('Reserva creada exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <Overlay
        isVisible={mostrarAlert}
        overlayStyle={{
          paddingVertical: 32,
          paddingHorizontal: 16,
          height: 'auto',
        }}>
        {mostrarAlert && mostrarAlertReserva()}
      </Overlay>
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          {perfil.includes('alumno') && (
            <Input label="alumno" value={alumno} disabled />
          )}
          {alumnoElegido && alumnoElegido.tipo === 'temporal' && (
            <View>
              <Input
                label="nombre"
                value={reserva.nombre}
                onChange={text => setReserva({...reserva, nombre: text})}
              />
              <Input
                label="apellido"
                value={reserva.apellido}
                onChange={text => setReserva({...reserva, apellido: text})}
              />
              <Input
                label="documento"
                value={reserva.documento}
                onChange={text => setReserva({...reserva, documento: text})}
              />
            </View>
          )}
          {perfil.includes('secretaria') && (
            <Select
              onValueChange={itemValue => {
                setAlumnoElegido(itemValue);
                setReserva({...reserva, idAlumno: itemValue.idAlumno});
              }}
              label={'alumno'}
              value={alumnoElegido}
              placeholder={'Elija un alumno'}
              items={alumnos?.map(a => {
                return {key: a.idAlumno, label: a.nombreCompleto, value: a};
              })}
            />
          )}
          <Input
            label="fecha"
            value={moment(reserva.fechaReserva).format('DD-MM-YYYY')}
            disabled
            onPress={() => setShowDatePicker(true)}
          />
          <Input
            label="inicio"
            value={moment(reserva.horaInicio).format('HH:mm')}
            disabled
            onPress={() => setShowTimePicker(true)}
          />
          {showDatePicker && (
            <DateTimePicker
              value={reserva.fechaReserva}
              mode={'date'}
              display="default"
              onChange={(event, date) => {
                setShowDatePicker(false);
                date && setReserva({...reserva, fechaReserva: date});
              }}
            />
          )}
          {showTimePicker && (
            <DateTimePicker
              value={reserva.horaInicio}
              mode={'time'}
              minuteInterval={15}
              is24Hour={true}
              display="default"
              onChange={(event, date) => {
                setShowTimePicker(false);
                date && setReserva({...reserva, horaInicio: date});
              }}
            />
          )}
          <Select
            onValueChange={itemValue => {
              setReserva({...reserva, bloque: itemValue});
            }}
            label={'bloque'}
            value={reserva.bloque}
            placeholder={'Elija un bloque'}
            items={[
              {key: 1, value: '1', label: '1'},
              {key: 2, value: '2', label: '2'},
            ]}
          />
          <Select
            onValueChange={itemValue => {
              setAeronaveElegida(itemValue);
            }}
            label={'aeronave'}
            value={aeronaveElegida}
            placeholder={'Elija una aeronave'}
            items={aeronaves?.map(a => {
              return {key: a.idAeronave, label: a.descripcion, value: a};
            })}
          />
          <Button
            title="crear reserva"
            margin
            onPress={() => setMostrarAlert(true)}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CrearReservaScreen;
