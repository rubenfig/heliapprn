import React, {useContext, useEffect, useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import {
  colors,
  elevationShadowStyle,
  perfectSize,
} from '../../../utils/styling';
import {Button as ElementsButton, Overlay} from 'react-native-elements';
import {Button} from '../../../components/Button';
import moment from 'moment';
import {UserContext} from '../../../utils/UserContext';
import {MisionesService} from '../../../api/misionesService';
import {Input} from '../../../components/Input';
import {ReservaService} from '../../../api/reservaService';
import {styles as itemListStyles} from '../../../components/ItemList';
import RNPickerSelect from 'react-native-picker-select';
import DetailRow from '../../../components/DetailRow';
const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    paddingVertical: 16,
    paddingHorizontal: 20,
    marginHorizontal: 20,
    marginTop: 32,
    ...elevationShadowStyle(4),
  },
  title: {
    marginVertical: 16,
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(26),
    lineHeight: perfectSize(32),
    textAlign: 'center',
    color: colors.primary,
    textTransform: 'uppercase',
  },
  subtitle: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(13),
    lineHeight: perfectSize(13),
    color: colors.secondary,
    textTransform: 'uppercase',
  },
  body: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(13),
    lineHeight: perfectSize(13),
    color: colors.gray,
  },
});
const DetalleReservaScreen = ({navigation, route}) => {
  const [reserva, setReserva] = useState(route?.params?.reserva);
  const [tipoRechazoElegido, setTipoRechazoElegido] = useState(null);
  const [motivoRechazo, setMotivoRechazo] = useState(null);
  const [mostrarAlertMotivo, setMostrarAlertMotivo] = useState(false);
  const [loading, setLoading] = useState(false);
  const [comentario, setComentario] = useState('');
  const [calificacion, setCalificacion] = useState(null);
  const [tiposRechazo, setTiposRechazo] = useState([]);
  const calificado = !!reserva.calificacion;
  const {user} = useContext(UserContext);
  const perfil = user.perfil.toLowerCase();
  const idAlumno = perfil.includes('alumno') && user?.alumno?.idAlumno;
  useEffect(() => {
    if (reserva.idMision) {
      obtenerMision();
    }
    listarTiposRechazo();
  }, []);

  const listarTiposRechazo = async () => {
    try {
      const response = await ReservaService.listarRechazo();
      setTiposRechazo(response?.data?.lista);
    } catch (e) {
      Alert.alert('Ocurrió un error indeterminado!');
    }
  };

  const rechazarReserva = async () => {
    try {
      const data = {
        idReserva: reserva.idReserva,
        idInstructor: reserva.instructor
          ? reserva.instructor.idInstructor
          : reserva.idInstructor,
        estadoReserva: 3,
        idMision: reserva.mision ? reserva.mision.idMision : reserva.idMision,
        motivoRechazo: perfil.includes('alumno')
          ? 'Rechazo Alumno'
          : motivoRechazo,
        tipoRechazo: perfil.includes('alumno')
          ? 7
          : tipoRechazoElegido.idTipoRechazo,
      };
      setLoading(true);
      const response = await ReservaService.modificarEstado(data);
      if (response.data && response.data.estado === 0) {
        Alert.alert('Reserva modificada exitosamente!');
        navigation.goBack();
        route?.params?.refresh && route?.params?.refresh();
      } else {
        Alert.alert(
          response?.data?.mensaje || 'Ocurrió un error indeterminado',
        );
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };
  const limpiarRechazo = () => {
    setTipoRechazoElegido(null);
    setMostrarAlertMotivo(false);
    setMotivoRechazo(null);
  };
  const calificar = async () => {
    const data = {
      puntuacion: calificacion,
      observacion: comentario,
      idReserva: reserva.idReserva,
    };
    console.log(data, reserva);
    const res = await ReservaService.calificarReserva(data);
    if (res?.data?.estado === 0) {
      console.log(res.data);
      Alert.alert('Reserva calificada exitosamente!');
      setReserva({...reserva, calificacion: calificacion});
      route?.params?.refresh && route?.params?.refresh();
    } else {
      Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
    }
  };
  const obtenerMision = async () => {
    setLoading(true);
    try {
      const listarFunc = await MisionesService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        for (const mision of response) {
          if (mision.idMision === reserva.idMision) {
            setReserva({...reserva, mision});
          }
        }
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.background,
      }}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <Overlay
        isVisible={!!mostrarAlertMotivo}
        overlayStyle={{
          paddingVertical: 32,
          paddingHorizontal: 16,
          height: 'auto',
        }}>
        {loading && (
          <ActivityIndicator
            style={[StyleSheet.absoluteFill, {zIndex: 10}]}
            size={'large'}
          />
        )}
        <Text style={[itemListStyles.titleStyle, {textAlign: 'center'}]}>
          Rechazar reserva
        </Text>
        <View
          style={{
            marginTop: 40,
          }}>
          <Text
            style={{
              color: '#666666',
              fontFamily: 'Questrial-Regular',
              fontSize: perfectSize(18),
              lineHeight: perfectSize(23),
            }}>
            Tipo de rechazo
          </Text>
          <RNPickerSelect
            onValueChange={itemValue => {
              setTipoRechazoElegido(itemValue);
            }}
            value={tipoRechazoElegido}
            placeholder={{
              label: 'Elija un tipo de rechazo',
              key: null,
              value: null,
            }}
            style={{
              inputIOS: {
                paddingVertical: 0,
                paddingRight: 30,
                borderBottomWidth: 1,
                borderBottomColor: '#E6E6E6',
                fontFamily: 'Questrial-Regular',
                fontSize: perfectSize(18),
                lineHeight: perfectSize(23),
              },
              inputAndroid: {
                paddingVertical: 0,
                paddingRight: 30,
                borderBottomWidth: 1,
                borderBottomColor: '#E6E6E6',
                fontFamily: 'Questrial-Regular',
                fontSize: perfectSize(18),
                lineHeight: perfectSize(23),
              },
            }}
            useNativeAndroidPickerStyle={false}
            items={tiposRechazo?.map(m => {
              return {key: m.idTipoRechazo, label: m.descripcion, value: m};
            })}
          />
        </View>
        <Input
          value={motivoRechazo}
          label="Motivo"
          onChange={setMotivoRechazo}
        />
        <Text style={[itemListStyles.subtitleStyle, {textAlign: 'center'}]}>
          Obs: se aplicaran recargos al cancelar una reserva
        </Text>
        <Button
          title={'Rechazar reserva'}
          containerStyle={{marginTop: 32}}
          onPress={() => rechazarReserva()}
        />
        <Button
          title={'Cancelar'}
          containerStyle={{marginTop: 32}}
          onPress={() => limpiarRechazo()}
        />
      </Overlay>
      <View style={styles.card}>
        <Text style={styles.title}>ID: {reserva?.idReserva}</Text>
        <DetailRow
          label={'alumno'}
          values={[reserva?.alumno?.nombreCompleto]}
        />
        <DetailRow
          label={'curso'}
          values={[reserva?.alumno?.cursoDescripcion]}
        />
        <DetailRow
          label={'instructor'}
          values={[reserva?.instructor?.nombreCompleto]}
        />
        <DetailRow
          label={'fecha'}
          values={[
            reserva.fechaReserva &&
              moment(reserva.fechaReserva).format('DD-MM-YYYY'),
          ]}
        />
        <DetailRow
          label={'horario'}
          values={[
            reserva.inicio &&
              reserva.fin &&
              moment(reserva.inicio).format('HH:mm') +
                '-' +
                moment(reserva.fin).format('HH:mm'),
          ]}
        />
        <DetailRow
          label={'horario de vuelo'}
          values={[
            reserva.spiderIni &&
              reserva.spiderFin &&
              moment(reserva.spiderIni).format('HH:mm') +
                '-' +
                moment(reserva.spiderFin).format('HH:mm'),
          ]}
        />
        <DetailRow
          label={'aeronave'}
          values={[reserva?.aeronave?.descripcion]}
        />
        <DetailRow label={'misión'} values={[reserva?.mision?.descripcion]} />
        <DetailRow label={'estado'} values={[reserva?.estado?.descripcion]} />
        {reserva.estadoReserva === 6 &&
          !calificado &&
          perfil.includes('alumno') && (
            <View style={{marginTop: 20}}>
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                {[1, 2, 3, 4, 5].map(i => (
                  <ElementsButton
                    key={i}
                    type={'clear'}
                    icon={{
                      name: 'star',
                      type: 'entypo',
                      color: i <= calificacion ? 'orange' : 'gray',
                    }}
                    onPress={() => setCalificacion(i)}
                  />
                ))}
              </View>
              <Input
                containerStyle={{
                  marginTop: 0,
                  marginVertical: 20,
                }}
                value={comentario}
                onChange={setComentario}
                placeholder={'Ingrese un comentario...'}
              />
              <Button
                title={'calificar'}
                style={{
                  marginVertical: 20,
                  marginHorizontal: 20,
                }}
                onPress={calificar}
              />
            </View>
          )}
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 32,
          left: 20,
          right: 20,
        }}>
        {reserva.estadoReserva === 6 && (
          <Button
            title={'ver en el mapa'}
            style={{
              marginHorizontal: 20,
            }}
          />
        )}
        {(reserva.estadoReserva === 1 || reserva.estadoReserva === 2) &&
          (!idAlumno || idAlumno === reserva.idAlumno) && (
            <Button
              title={'cancelar reserva'}
              style={{
                marginHorizontal: 20,
              }}
              onPress={() => {
                if (perfil.includes('secretaria')) {
                  setMostrarAlertMotivo(true);
                } else {
                  rechazarReserva();
                }
              }}
            />
          )}
      </View>
    </View>
  );
};

export default DetalleReservaScreen;
