import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  Text,
  StatusBar,
} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../../components/ItemList';
import {reservaEstados} from '../../../utils/utils';
import {ReservaService} from '../../../api/reservaService';
import moment from 'moment';
import {colors} from '../../../utils/styling';
import {UserContext} from '../../../utils/UserContext';

const ReservasAlumnoScreen = ({navigation}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [reservas, setReservas] = useState([]);
  const {user} = useContext(UserContext);

  useEffect(() => {
    listarReservas();
  }, []);

  const listarReservas = async () => {
    setLoading(true);
    try {
      console.log(user?.alumno?.idAlumno);
      const listarFunc = await ReservaService.listarTodasReservasAlumno(
        user?.alumno?.idAlumno,
      );
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        console.log(response);
        const nuevosReservas = response.lista.map(a => {
          for (const estado of reservaEstados) {
            if (estado.idEstadoReserva === a.estadoReserva) {
              a.estado = estado;
            }
          }
          return {
            ...a,
            id: a.idReserva,
            title: 'ID: ' + a.idReserva,
            subtitle: a.aeronave?.descripcion,
          };
        });
        setReservas(nuevosReservas);
        itemList?.current?.reset();
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const renderMoreInfo = reserva => {
    console.log(reserva);
    return (
      <View>
        <Text style={itemListStyles.subtitleStyle}>
          Desde: {reserva.inicio && moment(reserva.inicio).format('HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Hasta: {reserva.fin && moment(reserva.fin).format('HH:mm')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Fecha:{' '}
          {reserva.fechaReserva &&
            moment(reserva.fechaReserva).format('DD-MM-YYYY')}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Alumno: {reserva.alumno?.nombreCompleto}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Estado: {reserva.estado?.descripcion}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Instructor: {reserva.instructor?.nombreCompleto}
        </Text>
        <Text style={itemListStyles.subtitleStyle}>
          Misión: {reserva.mision?.descripcion}
        </Text>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ItemList
        ref={itemList}
        items={reservas}
        leftButton={'ver detalle'}
        onLeftPress={item => navigation.navigate('DetalleReserva', item)}
        renderMoreInfo={renderMoreInfo}
      />
    </View>
  );
};

export default ReservasAlumnoScreen;
