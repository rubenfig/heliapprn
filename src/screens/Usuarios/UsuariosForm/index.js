import React, {useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {colors, perfectSize} from '../../../utils/styling';
import {Button} from '../../../components/Button';
import {Input} from '../../../components/Input';
import Select from '../../../components/Select';
import {UserService} from '../../../api/userService';
import {InstructoresService} from '../../../api/instructoresService';
import {isEmail} from '../../../utils/utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    fontFamily: 'Kanit-Regular',
    fontSize: perfectSize(20),
    lineHeight: perfectSize(25),
    textAlign: 'center',
    textTransform: 'uppercase',
    color: colors.tint1,
    marginBottom: 16,
  },
  infoText: {
    fontFamily: 'Questrial-Regular',
    fontSize: perfectSize(18),
    lineHeight: perfectSize(25),
  },
});

const UsuariosFormScreen = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [perfil, setPerfil] = useState(
    editar
      ? {
          idPerfil: route.params?.usuario?.idPerfil,
          descripcion: route.params?.usuario?.descripcionPerfil,
        }
      : null,
  );
  const [perfiles, setPerfiles] = useState([]);
  const [instructor, setInstructor] = useState(
    editar ? route.params?.usuario?.instructor : null,
  );
  console.log(route.params?.usuario);
  const [instructores, setInstructores] = useState([]);
  const [editar] = useState(route.params.editar);
  const [usuario, setUsuario] = useState(
    editar
      ? {
          nombreCompleto: route.params?.usuario?.nombreCompleto,
          usuario: route.params?.usuario?.usuario,
          correo: route.params?.usuario?.correo,
          direccion: route.params?.usuario?.direccion,
          documento: route.params?.usuario?.documento,
          nroLicenciaVuelo: route.params?.usuario?.nroLicenciaVuelo,
          instructor: route.params?.usuario?.instructor,
        }
      : {
          nombreCompleto: '',
          usuario: '',
          correo: '',
          direccion: '',
          documento: '',
          nroLicenciaVuelo: '',
          instructor: '',
        },
  );

  React.useEffect(() => {
    return navigation.addListener('focus', () => {
      console.log('test');
      listarPerfiles();
      listarInstructores();
    });
  }, [navigation]);

  const listarPerfiles = async () => {
    setLoading(true);
    try {
      const listarFunc = await UserService.listarPerfiles();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setPerfiles(response);
        response.map(c => {
          c.idPerfil === route.params?.usuario?.idPerfil && setPerfil(c);
        });
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const listarInstructores = async () => {
    setLoading(true);
    try {
      const listarFunc = await InstructoresService.listar();
      if (listarFunc && listarFunc.data) {
        const response = listarFunc?.data?.lista;
        setInstructores(response);
        response.map(c => {
          c.idInstructor === route.params?.usuario?.idInstructor &&
            setInstructor(c);
        });
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  const showAlert = msg => {
    Alert.alert(msg);
  };

  const validar = async () => {
    if (!usuario.nombreCompleto || usuario.nombreCompleto.length === 0) {
      showAlert('El nombre es requerido');
      return;
    }
    if (!usuario.usuario || usuario.usuario.length === 0) {
      showAlert('El nombre de usuario es requerido');
      return;
    }
    if (!usuario.correo || usuario.correo.length === 0) {
      showAlert('El correo es requerido');
      return;
    }
    if (!isEmail(usuario.correo)) {
      showAlert('El correo no es válido');
      return;
    }
    if (!perfil) {
      showAlert('El perfil es requerido');
      return;
    }
    if (
      perfil?.descripcion?.toLowerCase().includes('instructor') &&
      !instructor
    ) {
      showAlert('El instructor es requerido');
      return;
    }
    if (
      perfil.descripcion.toLowerCase().includes('alumno') &&
      (!usuario.nroLicenciaVuelo || usuario.nroLicenciaVuelo.length === 0)
    ) {
      showAlert('El numero de licencia es requerido');
      return;
    }
    await editarUsuario();
  };

  const editarUsuario = async () => {
    try {
      setLoading(true);
      const newUsuario = {...usuario};
      newUsuario.idPerfil = perfil.idPerfil;
      delete newUsuario.perfil;
      delete newUsuario.instructor;
      if (
        perfil.descripcion === null ||
        !perfil.descripcion.toLowerCase().includes('alumno')
      ) {
        delete newUsuario.documento;
        delete newUsuario.direccion;
        delete newUsuario.nroLicenciaVuelo;
      }
      if (
        perfil.descripcion &&
        perfil.descripcion.toLowerCase().includes('instructor')
      ) {
        newUsuario.idInstructor = instructor.idInstructor;
      }
      console.log(newUsuario);
      let res;
      if (editar) {
        usuario.idUsuario = route.params?.usuario?.idUsuario;
        res = await UserService.modificarUsuario(newUsuario);
      } else {
        res = await UserService.crearUsuario(newUsuario);
      }
      if (res?.data?.estado === 0) {
        Alert.alert('Usuario editado exitosamente!');
        navigation.goBack();
        route?.params?.refresh();
      } else {
        Alert.alert(res?.data?.mensaje || 'Ocurrió un error indeterminado');
      }
    } catch (err) {
      console.log(err);
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <StatusBar backgroundColor={colors.primary} />
      <ScrollView>
        <View style={{paddingHorizontal: perfectSize(45)}}>
          <Input
            label="nombre completo"
            value={usuario.nombreCompleto}
            onChange={text => setUsuario({...usuario, nombreCompleto: text})}
          />
          <Input
            label="usuario"
            value={usuario.usuario}
            onChange={text => setUsuario({...usuario, usuario: text})}
          />
          <Input
            label="correo"
            value={usuario.correo}
            keyboardType={'email'}
            onChange={text => setUsuario({...usuario, correo: text})}
          />
          <Select
            disabled={editar}
            onValueChange={itemValue => {
              setPerfil(itemValue);
            }}
            label={'perfil'}
            value={perfil}
            placeholder={'Elija un perfil'}
            items={perfiles?.map(a => {
              return {key: a.idPerfil, label: a.descripcion, value: a};
            })}
          />
          {perfil?.descripcion?.toLowerCase().includes('instructor') && (
            <Select
              onValueChange={itemValue => {
                setInstructor(itemValue);
              }}
              label={'instructor'}
              value={instructor}
              placeholder={'Elija un instructor'}
              items={instructores?.map(a => {
                return {key: a.idInstructor, label: a.nombreCompleto, value: a};
              })}
            />
          )}
          {perfil?.descripcion?.toLowerCase().includes('alumno') && (
            <View>
              <Input
                label="dirección"
                value={usuario.direccion}
                onChange={text => setUsuario({...usuario, direccion: text})}
              />
              <Input
                label="documento"
                value={usuario.documento}
                onChange={text => setUsuario({...usuario, documento: text})}
              />
              {!editar && (
                <Input
                  label="licencia de vuelo"
                  value={usuario.nroLicenciaVuelo}
                  onChange={text =>
                    setUsuario({...usuario, nroLicenciaVuelo: text})
                  }
                />
              )}
            </View>
          )}
          <Button title="guardar usuario" margin onPress={() => validar()} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default UsuariosFormScreen;
