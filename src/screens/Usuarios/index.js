import React, {useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Alert,
  Text,
  Switch,
} from 'react-native';
import {ItemList, styles as itemListStyles} from '../../components/ItemList';
import {SearchBar} from 'react-native-elements';
import {colors} from '../../utils/styling';
import {UserService} from '../../api/userService';

const UsuariosListadoScreen = ({navigation}) => {
  const itemList = useRef();
  const [loading, setLoading] = useState(false);
  const [filtro, setFiltro] = useState(null);
  const [usuarios, setUsuarios] = useState([]);
  const [usuariosFiltradas, setUsuariosFiltradas] = useState([]);

  useEffect(() => {
    listarUsuarios();
    navigation.setParams({refresh: listarUsuarios});
  }, []);

  const renderMoreInfo = item => {
    return (
      <View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[itemListStyles.subtitleStyle, {flex: 1}]}>
            Habilitado
          </Text>
          <Switch
            value={item.habilitado}
            onValueChange={value => {
              const newUsuarios = usuarios.map(r => {
                return r.idUsuario === item.idUsuario
                  ? {...r, habilitado: value}
                  : r;
              });
              setUsuarios(newUsuarios);
              habilitarUsuario(item, value);
            }}
          />
        </View>
      </View>
    );
  };

  const resetearPassword = async usuario => {
    setLoading(true);
    const response = await UserService.resetearPassword({
      usuario: usuario.usuario,
    });
    if (response.data && response.data.estado === 0) {
      Alert.alert('Usuario modificado exitosamente!');
      await listarUsuarios();
    } else {
      Alert.alert(response?.data?.mensaje || 'Ocurrió un error indeterminado');
    }
  };

  const habilitarUsuario = async (usuario, value) => {
    const data = {
      idUsuario: usuario.idUsuario,
      habilitado: value,
    };
    setLoading(true);
    const response = await UserService.modificarUsuario(data);
    if (response.data && response.data.estado === 0) {
      Alert.alert('Usuario modificado exitosamente!');
      await listarUsuarios();
    } else {
      Alert.alert(response?.data?.mensaje || 'Ocurrió un error indeterminado');
    }
  };

  const listarUsuarios = async () => {
    setLoading(true);
    try {
      const listarFunc = await UserService.listarUsuarios(filtro);
      if (listarFunc && listarFunc.data) {
        const response = listarFunc.data;
        const nuevasUsuarios = response.lista.map(a => {
          return {
            ...a,
            id: a.idUsuario,
            title: a.usuario,
            subtitle: a.nombreCompleto + ', ' + a.descripcionPerfil,
          };
        });
        setUsuarios(nuevasUsuarios);
        setUsuariosFiltradas(nuevasUsuarios);
        itemList?.current?.reset();
      }
    } catch (err) {
      Alert.alert('Ocurrió un error indeterminado!');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading && (
        <ActivityIndicator
          style={[StyleSheet.absoluteFill, {zIndex: 10}]}
          size={'large'}
        />
      )}
      <SearchBar
        inputContainerStyle={{backgroundColor: colors.white}}
        containerStyle={{
          backgroundColor: colors.primary,
          borderBottomWidth: 0,
          borderTopWidth: 0,
        }}
        placeholder="Buscar..."
        onChangeText={setFiltro}
        onSubmitEditing={listarUsuarios}
        value={filtro}
      />
      <ItemList
        ref={itemList}
        items={usuariosFiltradas}
        leftButton={'resetear contraseña'}
        rightButton={'editar'}
        onLeftPress={item =>
          Alert.alert('Resetear la contraseña del usuario?', null, [
            {text: 'Cancelar'},
            {text: 'Ok', onPress: () => resetearPassword(item)},
          ])
        }
        onRightPress={item =>
          navigation.navigate('UsuariosForm', {
            editar: true,
            usuario: item,
            refresh: listarUsuarios,
          })
        }
        renderMoreInfo={renderMoreInfo}
      />
    </View>
  );
};

export default UsuariosListadoScreen;
